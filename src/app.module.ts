import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "./modules/Auth/auth.module";
import { ConfigModule } from "@nestjs/config";
import { HomeModule } from "./modules/Home/home.module";
import mongoConnectionString from "./core/mongo/mongoConnectionString";
import { Blueprint720Module } from "./modules/Blueprint720/blueprint720.module";
import { Blueprint720LayoutGroupingModule } from "./modules/Blueprint720LayoutGrouping/blueprint720LayoutGrouping.module";
import { Blueprint720EstateModule } from "./modules/Blueprint720Estate/blueprint720Estate.module";
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ".env",
    }),
    MongooseModule.forRoot(mongoConnectionString, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      connectionName: "Database",
    }),
    AuthModule,
    HomeModule,
    Blueprint720Module,
    Blueprint720LayoutGroupingModule,
    Blueprint720EstateModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
