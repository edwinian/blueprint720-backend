import { Injectable, Scope, Inject, HttpService } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateOptions, isValidObjectId, PaginateResult } from "mongoose";
import { ObjectId } from "mongodb";

// interfaces & models
import {
  Blueprint720EstateUpdateModel,
  Blueprint720EstateSearchModel,
  Blueprint720EstateCreateModel,
} from "./models";
import BaseService from "../../core/layers/BaseService";
import {
  Blueprint720EstateModel,
  Blueprint720Estate,
  Blueprint720EstateDoc,
} from "./interfaces/blueprint720Estate";
import localizeSchema from "../../core/mongo/localize.schema";
import { User } from "../Auth/interfaces/user";
import {
  FileSDK,
  UploadedFile,
} from "@oodles-dev/file-sdk/build/main/lib/file-sdk";
import { REQUEST } from "@nestjs/core";
import { Request } from "express";
import { Blueprint720LayoutGroupingService } from "../Blueprint720LayoutGrouping/blueprint720LayoutGrouping.service";
import { Blueprint720Service } from "../Blueprint720/blueprint720.service";
import { Blueprint720EstateUploadedIcon } from "./interfaces/blueprint720Estate.uploadedIcon";
import { Blueprint720Doc } from "../Blueprint720/interfaces/blueprint720";
import { Blueprint720LayoutGroupingDoc } from "../Blueprint720LayoutGrouping/interfaces/blueprint720LayoutGrouping";
import { LocalizeString } from "src/core/mongo/localize";
@Injectable({ scope: Scope.REQUEST })
export class Blueprint720EstateService extends BaseService<Blueprint720EstateModel> {
  constructor(
    @InjectModel("Blueprint720Estates")
    private readonly blueprint720EstateRepository: Blueprint720EstateModel,
    private readonly blueprint720LayoutGroupingService: Blueprint720LayoutGroupingService,
    private readonly blueprint720Service: Blueprint720Service,

    @Inject(REQUEST) private readonly request: Request,
    private httpService: HttpService
  ) {
    super(blueprint720EstateRepository);
  }

  protected async castQuery(
    search: Blueprint720EstateSearchModel
  ): Promise<{ [key: string]: any }> {
    // initiate query's $and array
    const queryAnd = [];

    if (search.q) {
      // generate a regex version
      const qMatch = new RegExp(`${search.q}`, "i");
      // search through all locale in names for this query search
      queryAnd.push({
        $or: [
          ...Object.keys(localizeSchema).map((locale) => ({
            [`name.${locale}`]: qMatch,
          })),
          { code: qMatch },
        ],
      });
    }

    if (search._ids?.length) {
      queryAnd.push({
        _id: { $in: search._ids.map((id) => new ObjectId(id)) },
      });
    }

    if (search.codes?.length) {
      queryAnd.push({ code: { $in: search.codes } });
    }

    if (search.isRecommended !== undefined) {
      queryAnd.push({ isRecommended: search.isRecommended });
    }

    if (search.isNewEstate !== undefined) {
      queryAnd.push({ isNewEstate: search.isNewEstate });
    }

    if (search.regions?.length) {
      queryAnd.push({
        $or: [
          { "address.properties.country": { $in: search.regions } },
          { "address.properties.state": { $in: search.regions } },
          { "address.properties.city": { $in: search.regions } },
          { "address.properties.district": { $in: search.regions } },
          {
            "address.properties.administrative_area_1": { $in: search.regions },
          },
        ],
      });
    }

    // return object optionally with $and field
    // $and performs a logical AND operation on an array and
    // selects the documents that satisfy all the expressions in the array
    return queryAnd.length ? { $and: queryAnd } : {};
  }

  protected async castAggregateQuery(search: Blueprint720EstateSearchModel) {
    const q = await this.castQuery(search);

    return [
      { $match: q },

      ...(search.buildingComplexTypes?.length
        ? [
            {
              $lookup: {
                from: "Blueprint720s",
                let: { estateCode: "$code" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          {
                            $eq: ["$address.properties.estate", "$$estateCode"],
                          },
                          {
                            $in: [
                              "$buildingComplexType",
                              search.buildingComplexTypes,
                            ],
                          },
                        ],
                      },
                    },
                  },
                ],
                as: "blueprints",
              },
            },
            {
              $match: { "blueprints.0": { $exists: true } },
            },
            {
              $unset: "blueprints",
            },
          ]
        : []),
    ];
  }

  public async generateUniqueName(
    name: LocalizeString,
    currentUser: User
  ): Promise<LocalizeString> {
    const lang = this.request.locale.current;

    // find blueprints with same name and with (number) behind the name
    // ex: blueprint name is "name", check for "name", "name (1)", "name (2)"
    const matchNameRegex = new RegExp(`^${name[lang]}(\s\((\d+)\))?`);
    const findBlueprintName = await this.blueprint720EstateRepository
      .find({
        [`name.${lang}`]: matchNameRegex,
        owner: currentUser._id.toHexString(),
      })
      .sort({ [`name.${lang}`]: -1 })
      .limit(1);

    if (findBlueprintName.length) {
      // check if the name has (number)
      // ex: "name (1)"
      // If it has, split it into an array of three parts:
      // ex: "name (1)" => ["name (1)", "name", "1"]
      const bpNameMatch = findBlueprintName[0].name[lang].match(
        /^(.*)\s\((\d)\)$/
      );

      // add (number + 1) after the name
      // otherwise, just add (1)
      name[lang] = bpNameMatch?.[2]
        ? `${bpNameMatch[1]} (${parseInt(bpNameMatch[2], 10) + 1})`
        : `${name[lang]} (1)`;
    }

    return name;
  }

  public async uploadFiles(
    files: { [key: string]: UploadedFile[] } | undefined,
    baseObj: Blueprint720EstateUploadedIcon,
    options?: {
      // used for fetching existing asset document. This allows
      // us to delete existing files that will be replaced
      updateId?: string;
      isArchived?: boolean;
      organization?: string;
    }
  ) {
    // define options
    const opts = { ...options };
    // get the header authorization
    const headerAuthorization = this.request.headers["authorization"];
    // instantiate fileSdk
    const fileSdk = await new FileSDK({
      headerAuthorization,
    });

    // if asset files are passed in, map form fields back to files
    if (files?.icon?.length > 0) {
      if (baseObj.icon?.file) {
        // if file already exists, delete previous first
        await fileSdk.deleteFile(baseObj.icon.file);
      }
      // upload icon image
      const uploadedFile = await fileSdk.uploadFileFromStream(
        files.icon[0].buffer,
        files.icon[0].size,
        {
          qualities: [0.75, 0.5, 0.25],
          blobName: files.icon[0].originalname,
          mimeType: files.icon[0].mimetype,
          isArchived: opts.isArchived,
          organization: opts.organization,
          bucketFilePath: `${process.env.FILE_SDK_BUCKET_FILE_PATH}-assets`,
        }
      );
      // update icon field with uploaded file info
      baseObj.icon = {
        file: uploadedFile._id,
        url: uploadedFile.compressions.find((c) => c.quality === 1)?.url,
        large: uploadedFile.compressions.find((c) => c.quality === 0.75)?.url,
        medium: uploadedFile.compressions.find((c) => c.quality === 0.5)?.url,
        small: uploadedFile.compressions.find((c) => c.quality === 0.25)?.url,
      };
    }
    // return updated baseObj
    return baseObj;
  }

  public async find(
    search: Blueprint720EstateSearchModel,
    paginate: PaginateOptions,
    populates?: string[]
  ): Promise<PaginateResult<Blueprint720Estate>> {
    let paginateResult;
    if (search.paginate === undefined || search.paginate) {
      const q = await this.castAggregateQuery(search);

      const aggregate = this.blueprint720EstateRepository.aggregate<Blueprint720EstateDoc>(
        q
      );
      paginateResult = await this.blueprint720EstateRepository.aggregatePaginate(
        aggregate,
        {
          ...paginate,
          ...(search.onlyId
            ? { projection: { _id: 1, __v: 1, updatedAt: 1 } }
            : {}),
          lean: true,
        }
      );
      if (populates) {
        paginateResult.docs = await this._populate(
          paginateResult.docs,
          populates
        );
      }
    } else {
      const findCursor = this.blueprint720EstateRepository.find(
        await this.castQuery(search)
      );
      if (search.onlyId) findCursor.select("_id __v updatedAt");
      paginateResult = await findCursor;
      if (populates) {
        paginateResult = await this._populate(paginateResult, populates);
      }
    }
    return paginateResult;
  }

  public async findOne(search: Blueprint720EstateSearchModel) {
    const q = await this.castAggregateQuery(search);
    const aggregate = await this.blueprint720EstateRepository
      .aggregate<Blueprint720Estate>(q)
      .limit(1);

    return aggregate[0] || null;
  }

  public async create(
    createEstate: Blueprint720EstateCreateModel,
    currentUser: User,
    files?: { [key: string]: UploadedFile[] }
  ): Promise<Blueprint720Estate> {
    if (files) await this.uploadFiles(files, createEstate);

    if (createEstate.name) {
      createEstate.name = await this.generateUniqueName(
        createEstate.name || { _l: "" },
        currentUser
      );
    }

    const createdEstate = await this.blueprint720EstateRepository.create(
      createEstate
    );

    return createdEstate;
  }

  public async update(
    _id: string | Blueprint720Estate["code"],
    updateEstate: Blueprint720EstateUpdateModel,
    files?: { [key: string]: UploadedFile[] },
    currentUser?: User,
    populates?: string[]
  ): Promise<Blueprint720Estate> {
    if (files) await this.uploadFiles(files, updateEstate);

    if (updateEstate.name) {
      updateEstate.name = await this.generateUniqueName(
        updateEstate.name || { _l: "" },
        currentUser
      );
    }

    if (updateEstate.address) {
      const {
        country,
        state,
        city,
        district,
        administrative_area_1,
        estate,
        building,
        floor,
        unit,
      } = updateEstate.address.properties;

      // find all layouts with reference to this estate
      const layoutsWithEstate = await this.blueprint720LayoutGroupingService.find(
        { estates: [updateEstate.name[`${this.request.locale.current}`]] },
        {},
        []
      );

      // update the layouts' group
      for (let layout of layoutsWithEstate as Blueprint720LayoutGroupingDoc[]) {
        await this.blueprint720LayoutGroupingService.update(
          layout._id.toHexString(),
          layout.groups.find((g) => g.estate === _id)._id.toHexString(),
          {
            administrative_area_1,
            building,
            city,
            country,
            district,
            estate,
            state,
            floorsFr: floor,
            floorsTo: floor,
            units: [unit],
          },
          currentUser
        );
      }
      // find all blueprints with reference to this estate
      const blueprintsWithEstate = await this.blueprint720Service.find(
        {
          addresses: [
            { estates: [updateEstate.name[`${this.request.locale.current}`]] },
          ],
        },
        {},
        currentUser
      );

      // update the blueprints' address
      for (let bp of blueprintsWithEstate as Blueprint720Doc[]) {
        await this.blueprint720Service.update(
          bp._id.toHexString(),
          {
            address: updateEstate.address,
          },
          {},
          currentUser
        );
      }
    }

    const updatedEstate = await this.blueprint720EstateRepository.findOneAndUpdate(
      {
        ...(isValidObjectId(_id) ? { _id } : { code: _id }),
        populates,
        currentUser,
      },
      updateEstate,
      { new: true }
    );

    return updatedEstate;
  }

  public async archive(
    _id: string | Blueprint720Estate["code"],
    isArchived = true,
    populates?: string[]
  ): Promise<Blueprint720Estate> {
    await this.blueprint720EstateRepository.findOneAndUpdate(
      {
        ...(isValidObjectId(_id) ? { _id } : { code: _id }),
      },
      { isArchived, new: true }
    );

    return this.findOne({ _ids: [_id], populates });
  }

  public async findByIdOrCode(
    id: string,
    populates: string[]
  ): Promise<Blueprint720Estate> {
    // get an estate by either _id or code
    let estate = await this.blueprint720EstateRepository.findOne({
      ...(ObjectId.isValid(id) ? { _id: new ObjectId(id) } : { code: id }),
    });

    // if estate is not found, just return null
    if (!estate) return null;

    // if requires populate, populate requested fields
    if (populates) {
      estate = await this._populate(estate, populates);
    }
    // return estate
    return estate;
  }
}
