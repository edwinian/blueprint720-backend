import { ObjectID } from "mongodb";
import { Document, PaginatesModel } from "mongoose";
import { Address } from "src/modules/Blueprint720/interfaces/blueprint720";
import { LocalizeString } from "../../../core/mongo/localize";

export interface Icon {
  file: string;
  url: string;
  large: string;
  medium: string;
  small: string;
}
export interface Blueprint720Estate {
  /**
   * unique ID for document
   */
  _id: ObjectID;

  code: string;

  icon?: Icon;

  name: LocalizeString;

  isRecommended?: boolean;

  isNewEstate?: boolean;

  isArchived?: boolean;

  address?: Address;

  /**
   * create time of this document
   */
  createdAt?: Date;

  /**
   * update time of this document
   */
  updatedAt?: Date;
}

export type Blueprint720EstateDoc = Blueprint720Estate & Document;

export type Blueprint720EstateModel = PaginatesModel<Blueprint720EstateDoc> &
  Document;
