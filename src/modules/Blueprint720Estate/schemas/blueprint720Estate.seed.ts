export default {
  model: "Blueprint720Estates",
  documents: [
    {
      _id: "606191706352546d039a13a0",
      code: "phase_1_the_metro_city",
      name: {
        en: "test estate 01",
        "zh-hk": "test estate 01 HK",
        "zh-cn": "test estate 01 CN",
      },
      icon: {
        file: "607664d868103952c9b6879d",
        url:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
        large:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
        medium:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
        small:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
      },
      isRecommended: true,
      isNewEstate: true,
      isArchived: false,
      buildingComplexType: "privateHousing",
      address: {
        geometry: {
          type: "Point",
          coordinates: [0, 0],
        },
        properties: {
          text: "新界 沙田 沙田第一城",
          country: "hk",
          state: "new_territory",
          city: "",
          district: "sha_tin_district",
          administrative_area_1: "sha_tin",
        },
        type: "Feature",
      },
      createdAt: "2020-08-01 02:19:12.334Z",
      updatedAt: "2020-08-01 02:19:12.334Z",
    },
    {
      _id: "60768dcbe5aca9a7676a1407",
      __v: 0,
      address: {
        geometry: {
          type: "Point",
          coordinates: [0, 0],
        },
        properties: {
          text: "新界 沙田 沙田第一城",
          country: "hk",
          state: "new_territory",
          city: "",
          district: "sha_tin_district",
          administrative_area_1: "sha_tin",
        },
        type: "Feature",
      },
      buildingComplexType: "privateHousing",
      code: "phase_2_the_metro_city",
      createdAt: "2021-04-14T06:13:25.609Z",
      icon: {
        file: "607664d868103952c9b6879d",
        url:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
        large:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
        medium:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
        small:
          "https://condonow.com/Panda-Condos/images/Panda-Condos-by-Lifetime-Developments-Downtown-Toronto-Condo-1-v79-full.jpg",
      },
      isNewEstate: true,
      isRecommended: true,
      name: {
        en: "test estate 02",
        "zh-hk": "test estate 02 HK",
        "zh-cn": "test estate 02 CN",
      },
      updatedAt: "2021-04-14T06:13:25.609Z",
    },
  ],
};
