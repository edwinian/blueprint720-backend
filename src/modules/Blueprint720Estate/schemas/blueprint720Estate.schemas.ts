import { Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import localizeSchema from "../../../core/mongo/localize.schema";
import mongooseAggregatePaginate from "mongoose-aggregate-paginate-v2";

export const Blueprint720EstateSchema = new Schema(
  {
    code: { type: String, unique: true, index: true, required: true },
    name: localizeSchema,
    icon: {
      type: {
        file: { type: String, required: true },
        url: { type: String, required: true },
        large: { type: String, required: true },
        medium: { type: String, required: true },
        small: { type: String, required: true },
      },
      required: false,
    },
    isRecommended: { type: Boolean, index: true },
    isNewEstate: { type: Boolean, index: true },
    isArchived: { type: Boolean, sparse: true },
    buildingComplexType: { type: String, index: true },
    address: {
      type: { type: String, default: "Feature" },
      geometry: {
        type: { type: String, default: "Point" },
        coordinates: { type: [Number], default: [0, 0] },
      },
      properties: {
        text: { type: String, default: "", sparse: true },
        country: { type: String, default: "", sparse: true },
        state: { type: String, default: "", sparse: true },
        city: { type: String, default: "", sparse: true },
        district: { type: String, default: "", sparse: true },
        administrative_area_1: { type: String, default: "", sparse: true },
        elevation_m: { type: Number, sparse: true },
      },
    },
  },
  {
    collection: "Blueprint720Estates",
    timestamps: true,
  }
);

Blueprint720EstateSchema.plugin(mongoosePaginate);
Blueprint720EstateSchema.plugin(mongooseAggregatePaginate);
