import { HttpModule, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { Blueprint720EstateSchema } from "./schemas/blueprint720Estate.schemas";
import { Blueprint720EstateController } from "./blueprint720Estate.controller";
import { Blueprint720EstateService } from "./blueprint720Estate.service";
import { Blueprint720LayoutGroupingModule } from "../Blueprint720LayoutGrouping/blueprint720LayoutGrouping.module";
import { Blueprint720Module } from "../Blueprint720/blueprint720.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Blueprint720Estates", schema: Blueprint720EstateSchema },
    ]),
    Blueprint720LayoutGroupingModule,
    Blueprint720Module,
    HttpModule,
  ],
  controllers: [Blueprint720EstateController],
  providers: [Blueprint720EstateService],
  exports: [Blueprint720EstateService],
})
export class Blueprint720EstateModule {}
