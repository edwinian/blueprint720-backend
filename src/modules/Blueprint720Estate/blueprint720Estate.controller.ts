import {
  Get,
  Query,
  UseGuards,
  Controller,
  UseInterceptors,
  Post,
  Body,
  UploadedFiles,
  Put,
  Param,
} from "@nestjs/common";
import { AuthGuard } from "../Auth/user.auth.guard";
import extractPaginate from "../../core/mongo/extractPaginate";

// services
import { Blueprint720EstateService } from "./blueprint720Estate.service";

// models
import {
  Blueprint720EstateCreateDTOModel,
  Blueprint720EstateSearchModel,
  Blueprint720EstateUpdateModel,
} from "./models";
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import { fileFilterMultiple } from "src/core/pipes/fileFilterMultiple";
import { ParseFormDataInterceptor } from "src/core/interceptors/ParseFormDataInterceptor";
import { CurrentUser } from "src/core/decorators/currentUser.decorator";
import { User } from "../Auth/interfaces/user";
import { UploadedFile } from "@oodles-dev/file-sdk/build/main/lib/file-sdk";
import { localeInterceptorFactory } from "@oodles-dev/nest-locale-module";
import { Blueprint720Estate } from "./interfaces";

const LocaleInterceptor = localeInterceptorFactory(["name"]);
@Controller("blueprint720-estates")
@UseGuards(AuthGuard({ optional: true }))
export class Blueprint720EstateController {
  constructor(
    private readonly blueprint720EstateService: Blueprint720EstateService
  ) {}

  // Routes
  // GET /blueprint720-estates
  // POST /blueprint720-estates
  // PUT /blueprint720-estates/:_id

  @Get()
  @UseInterceptors(LocaleInterceptor)
  public async find(@Query() query: Blueprint720EstateSearchModel) {
    const blueprint720Estate = await this.blueprint720EstateService.find(
      query,
      extractPaginate(query),
      query.populates
    );
    return blueprint720Estate;
  }

  @Post()
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "icon" }], {
      fileFilter: fileFilterMultiple({
        fieldName: ["icon"],
        extensions: ["jpg", "jpeg", "png"],
      }),
    }),
    ParseFormDataInterceptor,
    LocaleInterceptor
  )
  public async create(
    @CurrentUser() currentUser: User,
    @Body() body: Blueprint720EstateCreateDTOModel,
    @Query("populates") populates: string[],
    @UploadedFiles() files?: { [key: string]: UploadedFile[] }
  ) {
    if (Object.keys(body).length === 0) throw new Error("No create found");

    if (body.name) body.name._l = "";

    const createdEstate = await this.blueprint720EstateService.create(
      body,
      currentUser,
      files
    );

    return this.blueprint720EstateService._populate(createdEstate, populates);
  }

  @Put(":_id")
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "icon" }], {
      fileFilter: fileFilterMultiple({
        fieldName: ["icon"],
        extensions: ["jpg", "jpeg", "png"],
      }),
    }),
    ParseFormDataInterceptor,
    LocaleInterceptor
  )
  public async put(
    @Param("_id") _id: string | Blueprint720Estate["code"],
    @Body() body: Blueprint720EstateUpdateModel,
    @Query("populates") populates: string[],
    @CurrentUser() currentUser: User,
    @UploadedFiles() files?: { [key: string]: UploadedFile[] }
  ) {
    if (Object.keys(body).length === 0) throw new Error("No update found");

    if (body.name) body.name._l = "";

    const updatedEstate = await this.blueprint720EstateService.update(
      _id,
      body,
      files,
      currentUser,
      populates
    );

    return this.blueprint720EstateService._populate(updatedEstate, populates);
  }

  @Put(":_id/archive")
  @UseGuards(AuthGuard())
  public async archive(
    @Param("_id") _id: string | Blueprint720Estate["code"],
    @Query("isArchived") isArchived?: boolean
  ) {
    return this.blueprint720EstateService.archive(_id, isArchived);
  }
}
