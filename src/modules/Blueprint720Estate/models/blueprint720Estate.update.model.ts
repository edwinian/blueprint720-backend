import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import { LocalizeString } from "src/core/mongo/localize";
import { Address } from "src/modules/Blueprint720/interfaces/blueprint720";
import { Icon } from "../interfaces/blueprint720Estate";

export class Blueprint720EstateUpdateModel {
  @IsOptional()
  @IsString()
  code?: string;

  @IsOptional()
  name?: LocalizeString;

  @IsOptional()
  @ValidateNested()
  icon?: Icon;

  @IsOptional()
  @IsBoolean()
  isRecommended?: boolean;

  @IsOptional()
  @IsBoolean()
  isNewEstate?: boolean;

  @IsOptional()
  @ValidateNested()
  address?: Address;
}
