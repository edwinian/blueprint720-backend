import { Transform } from "class-transformer";
import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import transformBoolean from "src/core/classTransformers/transformBoolean";
import { BaseSearchModel } from "src/core/mongo/BaseSearchModel";

export class Blueprint720EstateSearchModel extends BaseSearchModel {
  @IsOptional()
  @IsString({ each: true })
  codes?: string[];

  @IsOptional()
  @IsString()
  q?: string;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isRecommended?: string | boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isNewEstate?: string | boolean;

  @IsOptional()
  @IsString({ each: true })
  buildingComplexTypes?: string[];

  @IsString({ each: true })
  @IsOptional()
  regions?: string[];
}
