import {
  Get,
  Put,
  Body,
  Post,
  Param,
  Query,
  Delete,
  UseGuards,
  Controller,
  UseInterceptors,
} from "@nestjs/common";
import { AuthGuard } from "../Auth/user.auth.guard";
import extractPaginate from "../../core/mongo/extractPaginate";

// services
import { Blueprint720LayoutGroupingService } from "./blueprint720LayoutGrouping.service";

// models
import {
  Blueprint720LayoutGroupingUpdateModel,
  Blueprint720LayoutGroupingSearchModel,
} from "./models";
import { CurrentUser } from "../../core/decorators/currentUser.decorator";
import { Blueprint720LayoutGroupingCreateDTOModel } from "./models/blueprint720LayoutGrouping.create.dto.model";
import { User } from "../Auth/interfaces/user";
import { LocaleInterceptor } from "@oodles-dev/nest-locale-module";

@Controller("blueprint720-layout-groupings")
@UseGuards(AuthGuard())
export class Blueprint720LayoutGroupingController {
  constructor(
    private readonly blueprint720LayoutGroupingService: Blueprint720LayoutGroupingService
  ) {}

  // Routes
  // GET /blueprint720LayoutGroupings
  // GET /blueprint720LayoutGroupings/:_id
  // POST /blueprint720LayoutGroupings
  // PUT /blueprint720LayoutGroupings/:_id/group/:groupId
  // DELETE /blueprint720LayoutGroupings/:_id

  @Get()
  @UseInterceptors(LocaleInterceptor)
  @UseGuards(AuthGuard({ optional: true }))
  public async find(
    @Query() query: Blueprint720LayoutGroupingSearchModel,
    @CurrentUser() currentUser?: User
  ) {
    const blueprint720LayoutGrouping = await this.blueprint720LayoutGroupingService.find(
      query,
      extractPaginate(query),
      query.populates,
      currentUser
    );
    return blueprint720LayoutGrouping;
  }

  @Get(":_id")
  @UseInterceptors(LocaleInterceptor)
  public async findById(@Param("_id") _id: string) {
    const pJ = await this.blueprint720LayoutGroupingService.findById(_id);
    return pJ;
  }

  @Post()
  @UseGuards(AuthGuard())
  public async create(
    @Body() body: Blueprint720LayoutGroupingCreateDTOModel,
    @CurrentUser() currentUser: User,
    @Query("populates") populates: string[]
  ) {
    const createdItem = await this.blueprint720LayoutGroupingService.create(
      body,
      currentUser
    );

    return this.blueprint720LayoutGroupingService._populate(
      createdItem,
      populates
    );
  }

  @Put(":_id/group/:groupId")
  @UseGuards(AuthGuard())
  public async updateBlueprint720LayoutGroupings(
    @Param("_id") _id: string,
    @Param("groupId") groupId: string,
    @Body() body: Blueprint720LayoutGroupingUpdateModel,
    @Query("populates") populates: string[],
    @CurrentUser() currentUser: User
  ) {
    // FIXME: files empty
    const updatedItem = await this.blueprint720LayoutGroupingService.update(
      _id,
      groupId,
      body,
      currentUser
    );
    return this.blueprint720LayoutGroupingService._populate(
      updatedItem,
      populates
    );
  }

  @Delete(":_id")
  @UseGuards(AuthGuard())
  public async delete(
    @Param("_id") _id: string,
    @Query("populates") populates: string[]
  ) {
    const deletedItem = await this.blueprint720LayoutGroupingService.delete(
      _id
    );
    return this.blueprint720LayoutGroupingService._populate(
      deletedItem,
      populates
    );
  }
}
