import { ObjectID } from "mongodb";
import { Document, PaginateModel } from "mongoose";

export interface Group {
  _id: ObjectID;
  country?: string;
  state?: string;
  city?: string;
  district?: string;
  administrative_area_1?: string;
  estate?: string;
  building?: string;
  floorsFr?: number;
  floorsTo?: number;
  units?: string[];
}

export interface Blueprint720LayoutGrouping {
  /**
   * unique ID for document
   */
  _id?: ObjectID;

  externalId?: string;

  groups: Group[];

  /**
   * create time of this document
   */
  createdAt?: Date;

  /**
   * update time of this document
   */
  updatedAt?: Date;
}

export type Blueprint720LayoutGroupingDoc = Blueprint720LayoutGrouping &
  Document;

export type Blueprint720LayoutGroupingModel = PaginateModel<Blueprint720LayoutGroupingDoc> &
  Document;
