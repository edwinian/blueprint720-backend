import { forwardRef, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { Blueprint720LayoutGroupingSchema } from "./schemas/blueprint720LayoutGrouping.schemas";
import { Blueprint720LayoutGroupingController } from "./blueprint720LayoutGrouping.controller";
import { Blueprint720LayoutGroupingService } from "./blueprint720LayoutGrouping.service";
import { Blueprint720Module } from "../Blueprint720/blueprint720.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: "Blueprint720LayoutGroupings",
        schema: Blueprint720LayoutGroupingSchema,
      },
    ]),
    forwardRef(() => Blueprint720Module),
  ],
  controllers: [Blueprint720LayoutGroupingController],
  providers: [Blueprint720LayoutGroupingService],
  exports: [Blueprint720LayoutGroupingService],
})
export class Blueprint720LayoutGroupingModule {}
