import {
  Injectable,
  Scope,
  NotFoundException,
  Inject,
  forwardRef,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateOptions, PaginateResult, Mongoose } from "mongoose";
import { ObjectId } from "mongodb";

// interfaces & models
import {
  Blueprint720LayoutGroupingCreateModel,
  Blueprint720LayoutGroupingUpdateModel,
  Blueprint720LayoutGroupingSearchModel,
} from "./models";
import BaseService from "../../core/layers/BaseService";
import {
  Blueprint720LayoutGroupingModel,
  Blueprint720LayoutGrouping,
  Group,
  Blueprint720LayoutGroupingDoc,
} from "./interfaces/blueprint720LayoutGrouping";
import { User } from "../Auth/interfaces/user";
import { REQUEST } from "@nestjs/core";
import { Request } from "express";
import { Blueprint720Service } from "../Blueprint720/blueprint720.service";
import { Blueprint720 } from "../Blueprint720/interfaces/blueprint720";

@Injectable({ scope: Scope.REQUEST })
export class Blueprint720LayoutGroupingService extends BaseService<Blueprint720LayoutGroupingDoc> {
  constructor(
    @InjectModel("Blueprint720LayoutGroupings")
    private readonly blueprint720LayoutGroupingRepository: Blueprint720LayoutGroupingModel,
    @Inject(forwardRef(() => Blueprint720Service))
    private readonly blueprint720Service: Blueprint720Service,
    @Inject(REQUEST) private request: Request
  ) {
    super(blueprint720LayoutGroupingRepository);
  }

  protected async castQuery(
    search: Blueprint720LayoutGroupingSearchModel,
    currentUser?: User
  ): Promise<{ [key: string]: any }> {
    // initiate query's $and array
    let queryAnd = [];

    const groupMatch: any = {};

    if (search.countries) {
      groupMatch.country = { $in: search.countries };
    }
    if (search.states) {
      groupMatch.state = { $in: search.states };
    }
    if (search.cities) {
      groupMatch.city = { $in: search.cities };
    }
    if (search.districts) {
      groupMatch.district = { $in: search.districts };
    }
    if (search.administrative_area_1s) {
      groupMatch.administrative_area_1 = { $in: search.administrative_area_1s };
    }
    if (search.estates) {
      groupMatch.estate = { $in: search.estates };
    }
    if (search.buildings) {
      groupMatch.building = { $in: search.buildings };
    }
    if (search.units) {
      groupMatch.units = { $in: search.units };
    }

    if (search.floors) {
      const sortedFloor = search.floors.sort();
      groupMatch.floorsFr = { $lte: sortedFloor[0] };
      groupMatch.floorsTo = { $gte: sortedFloor[sortedFloor.length - 1] };
    }

    if (Object.keys(groupMatch)) {
      queryAnd.push({ groups: { $elemMatch: groupMatch } });
    }

    // return object optionally with $and field
    // $and performs a logical AND operation on an array and
    // selects the documents that satisfy all the expressions in the array
    return queryAnd.length ? { $and: queryAnd } : {};
  }

  public async find(
    search: Blueprint720LayoutGroupingSearchModel,
    paginate: PaginateOptions,
    populates: string[],
    currentUser?: User
  ): Promise<
    | PaginateResult<Blueprint720LayoutGroupingDoc>
    | Blueprint720LayoutGroupingDoc[]
  > {
    const q = await this.castQuery(search, currentUser);

    const paginateResult = await this.blueprint720LayoutGroupingRepository.paginate(
      q,
      {
        ...paginate,
        lean: true,
      }
    );
    if (populates) {
      paginateResult.docs = await this._populate(
        paginateResult.docs,
        populates
      );
    }
    return paginateResult;
  }

  public async findOne(
    search: Blueprint720LayoutGroupingSearchModel
  ): Promise<Blueprint720LayoutGroupingDoc> {
    const q = await this.castQuery(search);
    return this.blueprint720LayoutGroupingRepository.findOne(q);
  }

  public async findById(_id: string): Promise<Blueprint720LayoutGroupingDoc> {
    return this.blueprint720LayoutGroupingRepository.findById(_id);
  }

  public async create(
    createItem: Blueprint720LayoutGroupingCreateModel,
    currentUser: User
  ): Promise<Blueprint720LayoutGroupingDoc> {
    const layoutGrouping = await this.blueprint720LayoutGroupingRepository.create(
      createItem
    );

    // find all blueprint720s that has address fit one criteria set of groups and has layoutGrouping = null
    const blueprints = await this.blueprint720Service.find(
      {
        addresses: createItem.groups.map((g) => ({
          countries: [g.country],
          states: [g.state],
          cities: [g.city],
          districts: [g.district],
          administrative_area_1s: [g.administrative_area_1],
          estates: [g.estate],
          buildings: [g.building],
          floorsFr: g.floorsFr,
          floorsTo: g.floorsTo,
          units: g.units,
        })),
        paginate: false,
        layoutGroupings: [null],
      },
      {},
      currentUser
    );

    // If blueprints found, update their layoutGrouping to this id

    if (blueprints) {
      this.blueprint720Service;
      this.blueprint720Service.addLayoutGrouping(
        (blueprints as Blueprint720[]).map((bp) => bp._id),
        layoutGrouping._id
      );
    }

    // create and return created obj
    return layoutGrouping;
  }

  public async createBatch(
    createItems: Blueprint720LayoutGroupingCreateModel[]
  ): Promise<Blueprint720LayoutGroupingDoc[]> {
    // create and return created obj
    return this.blueprint720LayoutGroupingRepository.insertMany(createItems);
  }

  public async update(
    _id: string,
    groupId: string,
    editGroup: Blueprint720LayoutGroupingUpdateModel,
    currentUser: User
  ): Promise<Blueprint720LayoutGroupingDoc> {
    // 1) Find original layout grouping from db
    const layoutGrouping = await this.blueprint720LayoutGroupingRepository.findById(
      _id
    );

    if (!layoutGrouping) {
      throw new NotFoundException("layout grouping not found");
    }

    // 2) Find blueprints linked with original layout grouping
    const blueprints = await this.blueprint720Service.find(
      { layoutGroupings: [_id], paginate: false },
      {},
      currentUser
    );

    // 3) Remove layoutGrouping from these blueprints
    if (blueprints) {
      this.blueprint720Service.removeLayoutGrouping(
        (blueprints as Blueprint720[]).map((bp) => bp._id),
        layoutGrouping._id
      );
    }

    // 4) Find and update the group from the layout grouping
    const group = layoutGrouping.groups.find((g) => g._id.equals(groupId));

    if (!group) {
      throw new NotFoundException("group not found in the layout grouping");
    }

    Object.keys(editGroup).forEach((editGroupKey) => {
      group[editGroupKey] = editGroup[editGroupKey];
    });

    // 5) find all blueprints with no layout grouping and with address that fits one group of the layout grouping
    // // find all blueprint720s that has address fit one criteria set of groups and has layoutGrouping = null
    const blueprint720s = await this.blueprint720Service.find(
      {
        addresses: layoutGrouping.groups.map((g) => ({
          countries: [g.country],
          states: [g.state],
          cities: [g.city],
          districts: [g.district],
          administrative_area_1s: [g.administrative_area_1],
          estates: [g.estate],
          buildings: [g.building],
          floorsFr: g.floorsFr,
          floorsTo: g.floorsTo,
          units: g.units,
        })),
        paginate: false,
        layoutGroupingsNotEqual: [layoutGrouping._id.toHexString()],
      },
      {},
      currentUser
    );

    // 6) If blueprints found, update their layoutGrouping to this id

    if (blueprint720s) {
      this.blueprint720Service.addLayoutGrouping(
        (blueprint720s as Blueprint720[]).map((bp) => bp._id),
        layoutGrouping._id
      );
    }

    return this.blueprint720LayoutGroupingRepository.findByIdAndUpdate(
      layoutGrouping._id,
      { groups: layoutGrouping.groups },
      { new: true }
    );
  }

  public async delete(_id: string) {
    // find blueprint720LayoutGrouping from db
    const blueprint720LayoutGrouping = await this.blueprint720LayoutGroupingRepository.findById(
      _id
    );
    // if blueprint720LayoutGrouping not found, throw not found error
    if (!blueprint720LayoutGrouping) {
      throw new NotFoundException("blueprint720LayoutGrouping not found");
    }
    // delete and return blueprint720LayoutGrouping
    return this.blueprint720LayoutGroupingRepository.findByIdAndDelete(_id);
  }

  public async deleteMultiple(_ids: string[]) {
    // get blueprint720LayoutGrouping documents from db
    const blueprint720LayoutGroupingList = await this.blueprint720LayoutGroupingRepository.find(
      {
        _id: { $in: _ids },
      }
    );
    // if blueprint720LayoutGroupingList length not match, throw error
    if (blueprint720LayoutGroupingList.length !== _ids.length) {
      const foundIds = blueprint720LayoutGroupingList.map((f) =>
        f._id.toHexString()
      );
      throw new NotFoundException(
        `the following blueprint720LayoutGrouping were not found: ${_ids.filter(
          (id) => !foundIds.includes(id)
        )}`
      );
    }
    // remove from db
    await this.blueprint720LayoutGroupingRepository.deleteMany({
      _id: { $in: blueprint720LayoutGroupingList.map((f) => f._id) },
    });
    // return removed blueprint720LayoutGroupingList
    return blueprint720LayoutGroupingList;
  }
}
