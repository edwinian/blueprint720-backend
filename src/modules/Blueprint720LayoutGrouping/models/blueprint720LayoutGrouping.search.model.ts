import { IsNumber, IsOptional, IsString } from "class-validator";
import { BaseSearchModel } from "src/core/mongo/BaseSearchModel";

export class Blueprint720LayoutGroupingSearchModel extends BaseSearchModel {
  @IsOptional()
  @IsString({ each: true })
  countries?: string[];
  @IsOptional()
  @IsString({ each: true })
  states?: string[];
  @IsOptional()
  @IsString({ each: true })
  cities?: string[];
  @IsOptional()
  @IsString({ each: true })
  districts?: string[];
  @IsOptional()
  @IsString({ each: true })
  administrative_area_1s?: string[];
  @IsOptional()
  @IsString({ each: true })
  estates?: string[];
  @IsOptional()
  @IsString({ each: true })
  buildings?: string[];
  @IsOptional()
  @IsNumber({}, { each: true })
  floors?: number[];
  @IsOptional()
  @IsString({ each: true })
  units?: string[];
}
