import { IsNumber, IsOptional, IsString } from "class-validator";

export class Blueprint720LayoutGroupingUpdateModel {
  @IsOptional()
  @IsString()
  externalId?: string;
  @IsOptional()
  @IsString()
  country?: string;
  @IsOptional()
  @IsString()
  state?: string;
  @IsOptional()
  @IsString()
  city?: string;
  @IsOptional()
  @IsString()
  district?: string;
  @IsOptional()
  @IsString()
  administrative_area_1?: string;
  @IsOptional()
  @IsString()
  estate?: string;
  @IsOptional()
  @IsString()
  building?: string;
  @IsOptional()
  @IsNumber()
  floorsFr?: number;
  @IsOptional()
  @IsNumber()
  floorsTo?: number;
  @IsOptional()
  @IsString({ each: true })
  units?: string[];
}
