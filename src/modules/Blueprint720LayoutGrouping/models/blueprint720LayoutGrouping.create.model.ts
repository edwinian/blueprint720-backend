import { IsOptional, IsString, ValidateNested } from "class-validator";
import { Group } from "../interfaces/blueprint720LayoutGrouping";
export class Blueprint720LayoutGroupingCreateModel {
  @IsOptional()
  @IsString()
  externalId?: string;

  @ValidateNested({ each: true })
  groups: Group[];
}
