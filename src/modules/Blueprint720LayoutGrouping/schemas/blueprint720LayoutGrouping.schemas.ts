import { Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";

export const Blueprint720LayoutGroupingSchema = new Schema(
  {
    externalId: { type: String, unique: true, sparse: true, index: true },
    groups: {
      type: [
        {
          country: { type: String },
          state: { type: String },
          city: { type: String },
          district: { type: String },
          administrative_area_1: { type: String },
          estate: { type: String },
          building: { type: String },
          floorsFr: { type: Number },
          floorsTo: { type: Number },
          units: [{ type: String }],
        },
      ],
      required: true,
    },
  },
  {
    collection: "Blueprint720LayoutGroupings",
    timestamps: true,
  }
);

Blueprint720LayoutGroupingSchema.plugin(mongoosePaginate);
