export default {
  model: "Blueprint720LayoutGroupings",
  documents: [
    {
      _id: "1f2bbc792bd5330c52ddabf1",
      externalId: "test1",
      groups: [
        {
          country: "test",
          state: "test",
          city: "test",
          district: "test",
          administrative_area_1: "test",
          estate: "test",
          building: "test",
          floorsFr: 0,
          floorsTo: 1,
          units: ["test"],
        },
      ],
      createdAt: "2020-08-01 02:19:12.334Z",
      updatedAt: "2020-08-01 02:19:12.334Z",
    },
    {
      _id: "2f2bbc792bd5330c52ddabf1",
      externalId: "test2",
      groups: [
        {
          country: "test999",
          state: "test999",
          city: "test999",
          district: "test999",
          administrative_area_1: "test999",
          estate: "test999",
          building: "test999",
          floorsFr: 0,
          floorsTo: 2,
          units: ["test999"],
        },
      ],
      createdAt: "2020-08-02 02:19:12.334Z",
      updatedAt: "2020-08-02 02:19:12.334Z",
    },
  ],
};
