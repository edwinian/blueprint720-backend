import {
  Injectable,
  Scope,
  NotFoundException,
  Inject,
  HttpService,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import {
  PaginateOptions,
  AggregatePaginateResult,
  isValidObjectId,
} from "mongoose";
import { ObjectId } from "mongodb";
import * as jsondiffpatch from "jsondiffpatch";
import qs from "qs";

// interfaces & models
import {
  Blueprint720CreateModel,
  Blueprint720UpdateModel,
  Blueprint720SearchModel,
  EditUserPermissionModel,
  UpdateAssetModel,
} from "./models";
import BaseService from "../../core/layers/BaseService";
import { Blueprint720UploadedFiles } from "./interfaces/blueprint720.uploadedFiles";
import {
  Blueprint720Model,
  Blueprint720,
  UserPermissions,
  Data,
  Room,
  Asset,
  Blueprint720Doc,
} from "./interfaces/blueprint720";
import localizeSchema from "../../core/mongo/localize.schema";
import moment from "moment";
import { User } from "../Auth/interfaces/user";
import {
  UploadedFile,
  FileSDK,
  UploadedFiles,
} from "@oodles-dev/file-sdk/build/main/lib/file-sdk";
import { REQUEST } from "@nestjs/core";
import { Request } from "express";
import { Blueprint720LayoutGroupingService } from "../Blueprint720LayoutGrouping/blueprint720LayoutGrouping.service";
import { Blueprint720ShakeDataFiltersModel } from "./models/blueprint720.shakeData.filters.model";
import { LocalizeModel } from "src/core/mongo/localize.model";
import { AutocompleteSearchModel } from "./models/autocomplete.search.model";
import algoliasearch from "algoliasearch";
import { convertFieldsToLocalizeObject } from "@oodles-dev/nest-locale-module";
import { Blueprint720LayoutGrouping } from "../Blueprint720LayoutGrouping/interfaces/blueprint720LayoutGrouping";

const client = algoliasearch(
  process.env.ALGOLIA_APPLICATION_ID,
  process.env.ALGOLIA_ADMIN_KEY
);

const index = client.initIndex(`${process.env.NODE_ENV}_blueprint720`);

@Injectable({ scope: Scope.REQUEST })
export class Blueprint720Service extends BaseService<Blueprint720Model> {
  Blueprint720LayoutGroupingService: Blueprint720LayoutGroupingService;
  constructor(
    @InjectModel("Blueprint720s")
    private readonly blueprint720Repository: Blueprint720Model,
    private readonly blueprint720LayoutGroupingService: Blueprint720LayoutGroupingService,
    @Inject(REQUEST) private readonly request: Request,
    private httpService: HttpService
  ) {
    super(blueprint720Repository);
  }

  protected async isAuthorized(
    currentUser: User,
    bp: Blueprint720,
    permission: "read" | "edit" | "delete"
  ) {
    if (
      currentUser._id !== bp.owner &&
      !bp.userPermissions
        .filter((up) => up[`${permission}`])
        .map((up) => up.user)
        .includes(currentUser._id)
    ) {
      throw new Error("unauthorized");
    }
  }

  protected async saveToAlgolia(_bpInDB: Blueprint720Doc) {
    // convert to normal object to get rid of Mongoose cursors
    const bpInDB = _bpInDB.toJSON ? _bpInDB.toJSON() : _bpInDB;

    // add/update the following to Algolia index
    const objectID = bpInDB._id;
    const {
      assets,
      bookmarkedUsers,
      externalId,
      layoutGroupings,
      isLayout,
      isNewEstate,
      isRecommended,
      rooms,
      owner,
      userPermissions,
      address,
      buildingComplexType,
      shapes,
      yearBuilt,
      sqft,
      sqftActual,
      styles,
      isArchived,
      isPublic,
      isSystem,
      createdAt,
      updatedAt,
    } = bpInDB;

    const localeSchemaToAlgolia = (
      val: LocalizeModel,
      fieldName: string
    ): { [key: string]: string } => {
      return Object.keys(val || {}).reduce(
        (obj: { [key: string]: string }, lang: string) => {
          if (lang === "_l") return obj;
          if (val?.[lang]) {
            obj[`${fieldName}_${lang}`] = val[lang];
          }
          return obj; // ex: name => {name_en: "value"}
        },
        {}
      );
    };

    const icon =
      assets.find((a) => a.type === "render")?.small ||
      assets.find((a) => a.type === "layout")?.small;

    index.partialUpdateObject(
      {
        objectID,
        ...(icon ? { icon } : {}),
        bookmarkedUsers,
        externalId,
        layoutGroupings,
        isLayout,
        isNewEstate,
        isRecommended,
        rooms,
        owner,
        userPermissions,
        address,
        buildingComplexType,
        shapes,
        yearBuilt,
        sqft,
        sqftActual,
        styles,
        isArchived,
        isPublic,
        isSystem,

        ...localeSchemaToAlgolia(bpInDB.name, "name"),
        ...localeSchemaToAlgolia(bpInDB.description, "description"),
        ...localeSchemaToAlgolia(bpInDB.ideaDescription, "ideaDescription"),

        // convert to unix timestamp
        createdAt: createdAt.getTime(),
        updatedAt: updatedAt.getTime(),
      },
      {
        createIfNotExists: true,
      }
    );
  }

  // ensure same user won't have blueprint with same names
  //TODO: when blueprints are categorized by folder, just make sure names in
  // the same folder don't duplicate
  protected async generateUniqueName(
    name: LocalizeModel,
    currentUser: User
  ): Promise<LocalizeModel> {
    const lang = this.request.locale.current;

    // find blueprints with same name and with (number) behind the name
    // ex: blueprint name is "name", check for "name", "name (1)", "name (2)"
    const matchNameRegex = new RegExp(`^${name[lang]}(\s\((\d+)\))?`);
    const findBlueprintName = await this.blueprint720Repository
      .find({
        [`name.${lang}`]: matchNameRegex,
        owner: currentUser._id.toHexString(),
      })
      .sort({ [`name.${lang}`]: -1 })
      .limit(1);

    if (findBlueprintName.length) {
      // check if the name has (number)
      // ex: "name (1)"
      // If it has, split it into an array of three parts:
      // ex: "name (1)" => ["name (1)", "name", "1"]
      const bpNameMatch = findBlueprintName[0].name[lang].match(
        /^(.*)\s\((\d)\)$/
      );

      // add (number + 1) after the name
      // otherwise, just add (1)
      name[lang] = bpNameMatch?.[2]
        ? `${bpNameMatch[1]} (${parseInt(bpNameMatch[2], 10) + 1})`
        : `${name[lang]} (1)`;
    }

    return name;
  }

  public getRoomsFromData = (
    data: Data,
    existingRooms: Room[] = []
  ): Room[] => {
    // generate rooms based on data
    const newRooms =
      data?.floorplan?.map((fp) => ({
        roomId: fp._id || "",
        types: fp.floorType?.length ? [fp.floorType] : [],
        spaces: fp.space?.length ? [fp.space] : [],
      })) || [];
    // filter existingRooms
    const customRooms = existingRooms.filter((er) => !er.roomId);
    // return merged rooms
    return [...customRooms, ...newRooms];
  };

  protected splitExternalIdsAndIds(ids: string[]) {
    return ids.reduce(
      (q, id) => {
        if (!id) return q;
        q[isValidObjectId(id) ? "_ids" : "externalIds"].push(id);
        return q;
      },
      { _ids: [], externalIds: [] }
    );
  }

  protected async castQuery(
    search: Blueprint720SearchModel,
    currentUser?: User
  ): Promise<{ [key: string]: any }> {
    // initiate query's $and array
    let queryAnd = [];

    if (search._ids?.length) {
      const idsQuery = this.splitExternalIdsAndIds(search._ids);

      queryAnd.push({
        $or: [
          ...(idsQuery._ids.length
            ? [{ _id: { $in: idsQuery._ids.map((id) => new ObjectId(id)) } }]
            : []),
          ...(idsQuery.externalIds.length
            ? [{ externalId: { $in: idsQuery.externalIds } }]
            : []),
        ],
      });
    }

    // if (search.q) {
    //   // FIXME: still a very loose way to search, need rankings
    //   // generate a regex version
    //   const qMatch = search.q
    //     .split(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/\s]+/)
    //     .filter((q) => q)
    //     .map((qPart) => new RegExp(`${qPart}`, "i"))
    //     .concat([new RegExp(search.q, "i")]);
    //   // search through all locale in names for this query search
    //   queryAnd.push({
    //     $or: [
    //       ...Object.keys(localizeSchema).map((locale) => ({
    //         [`name.${locale}`]: { $in: qMatch },
    //       })),
    //       ...Object.keys(localizeSchema).map((locale) => ({
    //         [`ideaDescription.${locale}`]: qMatch,
    //         [`description.${locale}`]: qMatch,
    //       })),
    //       { externalId: { $in: qMatch } },
    //       { "address.properties.text": { $in: qMatch } },
    //       { "address.properties.country": { $in: qMatch } },
    //       { "address.properties.state": { $in: qMatch } },
    //       { "address.properties.city": { $in: qMatch } },
    //       { "address.properties.district": { $in: qMatch } },
    //       { "address.properties.administrative_area_1": { $in: qMatch } },
    //       { "address.properties.estate": { $in: qMatch } },
    //       { "address.properties.building": { $in: qMatch } },
    //     ],
    //   });
    // }

    if (search.layoutGroupings?.length) {
      queryAnd.push({
        layoutGroupings: { $in: search.layoutGroupings },
      });
    }

    if (search.layoutGroupingsNotEqual?.length) {
      queryAnd.push({
        layoutGroupings: { $nin: search.layoutGroupingsNotEqual },
      });
    }

    if (search.assetTypes?.length) {
      queryAnd.push({
        "assets.type": { $in: search.assetTypes },
      });
    }

    if (search.externalIds?.length) {
      queryAnd.push({
        externalId: new RegExp(`(${search.externalIds.join("|")})`),
      });
    }

    if (search.buildingComplexTypes?.length) {
      queryAnd.push({
        buildingComplexType: { $in: search.buildingComplexTypes },
      });
    }

    if (search.regions?.length) {
      queryAnd.push({
        $or: [
          { "address.properties.country": { $in: search.regions } },
          { "address.properties.state": { $in: search.regions } },
          { "address.properties.city": { $in: search.regions } },
          { "address.properties.district": { $in: search.regions } },
          {
            "address.properties.administrative_area_1": { $in: search.regions },
          },
          { "address.properties.estate": { $in: search.regions } },
          { "address.properties.building": { $in: search.regions } },
        ],
      });
    }

    queryAnd.push({
      isArchived: search.isArchived ? true : { $ne: true },
    });

    if (search.isNewEstate !== undefined) {
      queryAnd.push({
        isNewEstate: search.isNewEstate ? true : { $ne: true },
      });
    }

    if (search.isRecommended !== undefined) {
      queryAnd.push({
        isRecommended: search.isRecommended ? true : { $ne: true },
      });
    }

    if (search.dateFr !== undefined) {
      queryAnd.push({
        createdAt: { $gte: moment(search.dateFr).startOf("day") },
      });
    }

    if (search.dateTo !== undefined) {
      queryAnd.push({
        createdAt: { $lte: moment(search.dateTo).endOf("day") },
      });
    }

    if (search.sqftMin !== undefined) {
      queryAnd.push({
        sqft: { $gte: search.sqftMin },
      });
    }

    if (search.sqftMax !== undefined) {
      queryAnd.push({
        sqft: { $lte: search.sqftMax },
      });
    }

    if (search.sqftActualMin !== undefined) {
      queryAnd.push({
        sqftActual: { $gte: search.sqftActualMin },
      });
    }

    if (search.sqftActualMax !== undefined) {
      queryAnd.push({
        sqftActual: { $lte: search.sqftActualMax },
      });
    }

    if (search.yearBuiltMin !== undefined) {
      queryAnd.push({
        yearBuilt: { $gte: search.yearBuiltMin },
      });
    }

    if (search.yearBuiltMax !== undefined) {
      queryAnd.push({
        yearBuilt: { $lte: search.yearBuiltMax },
      });
    }

    // ensure only retrieve blueprints that belongs to user or is public
    if (!search.fetchAll) {
      queryAnd.push({
        $or: [
          ...(currentUser ? [{ owner: currentUser._id }] : []),
          {
            isPublic: true,
          },
          {
            isSystem: true,
          },
        ],
      });
    }

    if (search.isLayout !== undefined) {
      queryAnd.push({
        isLayout: search.isLayout ? search.isLayout : { $ne: true },
      });
    }

    if (search.isPublic !== undefined) {
      queryAnd.push({
        isPublic: search.isPublic ? search.isPublic : { $ne: true },
      });
    }

    if (search.isSystem !== undefined) {
      queryAnd.push({
        isSystem: search.isSystem ? search.isSystem : { $ne: true },
      });
    }

    if (search.me !== undefined) {
      if (!currentUser) throw new Error("no user found");
      // explicitly set owner to this user
      queryAnd.push({
        owner: search.me ? currentUser._id : { $ne: currentUser._id },
      });
    }

    if (search.isBookmarked !== undefined) {
      queryAnd.push({
        bookmarkedUsers: {
          $elemMatch: {
            user: search.isBookmarked
              ? currentUser._id
              : { $ne: currentUser._id },
          },
        },
      });
    }

    if (search.bookmarkedBy?.length) {
      queryAnd.push({
        bookmarkedUsers: { $elemMatch: { user: { $in: search.bookmarkedBy } } },
      });
    }

    if (search.roomTypes?.length) {
      for (let roomType of search.roomTypes) {
        queryAnd.push({
          rooms: {
            $elemMatch: {
              types: roomType.type,
            },
          },
        });
      }
    }

    if (search.roomSpaces?.length) {
      search.roomSpaces.forEach((roomSpaces) => {
        queryAnd.push({
          rooms: {
            $elemMatch: {
              types: roomSpaces.type,
            },
          },
        });
      });
    }

    if (search.shapes?.length) {
      console.log("SHAPES?", search.shapes);

      queryAnd.push({
        shapes: { $in: search.shapes },
      });
    }

    if (search.styles?.length) {
      queryAnd.push({
        styles: { $in: search.styles },
      });
    }

    return queryAnd.length ? { $and: queryAnd } : {};
  }

  protected async castAggregateQuery(
    search: Blueprint720SearchModel,
    currentUser?: User
  ) {
    const q = await this.castQuery(search, currentUser);

    const roomTypeCounts = (search.roomTypes || []).filter(
      (r) => r.count !== undefined
    );
    const roomSpaceCounts = (search.roomSpaces || []).filter(
      (r) => r.count !== undefined
    );

    const sort = this.castSort(search.sortBy);

    const addressesMatch = (search.addresses || []).map((addr) => ({
      $or: [
        {
          "_layoutGroupings.groups": {
            $elemMatch: {
              $and: [
                ...(addr.countries
                  ? [
                      {
                        [`country`]: {
                          $in: addr.countries,
                        },
                      },
                    ]
                  : []),
                ...(addr.states
                  ? [
                      {
                        [`state`]: { $in: addr.states },
                      },
                    ]
                  : []),
                ...(addr.cities
                  ? [
                      {
                        [`city`]: { $in: addr.cities },
                      },
                    ]
                  : []),
                ...(addr.districts
                  ? [
                      {
                        [`district`]: { $in: addr.districts },
                      },
                    ]
                  : []),
                ...(addr.administrative_area_1s
                  ? [
                      {
                        [`administrative_area_1`]: {
                          $in: addr.administrative_area_1s,
                        },
                      },
                    ]
                  : []),
                ...(addr.estates
                  ? [
                      {
                        [`estate`]: { $in: addr.estates },
                      },
                    ]
                  : []),
                ...(addr.buildings
                  ? [
                      {
                        [`building`]: { $in: addr.buildings },
                      },
                    ]
                  : []),
                ...(addr.floorsFr !== undefined
                  ? [
                      {
                        [`floorsFr`]: { $lte: addr.floorsFr },
                      },
                    ]
                  : []),
                ...(addr.floorsTo !== undefined
                  ? [
                      {
                        [`floorsTo`]: { $gte: addr.floorsTo },
                      },
                    ]
                  : []),
                ...(addr.floors?.length
                  ? [
                      {
                        $or: addr.floors.map((floor) => ({
                          floorsFr: { $lte: parseInt(floor.toString(), 10) },
                          floorsTo: { $gte: parseInt(floor.toString(), 10) },
                        })),
                      },
                    ]
                  : []),
                ...(addr.units?.length
                  ? [
                      {
                        unit: {
                          $in: addr.units.map((u) => new RegExp(`^${u}$`, "i")),
                        },
                      },
                    ]
                  : []),
              ],
            },
          },
        },
        {
          ...(addr.countries && {
            [`address.properties.country`]: { $in: addr.countries },
          }),
          ...(addr.states && {
            [`address.properties.state`]: { $in: addr.states },
          }),
          ...(addr.cities && {
            [`address.properties.city`]: { $in: addr.cities },
          }),
          ...(addr.districts && {
            [`address.properties.district`]: { $in: addr.districts },
          }),
          ...(addr.administrative_area_1s && {
            [`address.properties.administrative_area_1`]: {
              $in: addr.administrative_area_1s,
            },
          }),
          ...(addr.estates && {
            [`address.properties.estate`]: { $in: addr.estates },
          }),
          ...(addr.buildings && {
            [`address.properties.building`]: { $in: addr.buildings },
          }),
          ...(addr.floorsFr !== undefined && {
            [`address.properties.floor`]: { $gte: addr.floorsFr },
          }),
          ...(addr.floorsTo !== undefined && {
            [`address.properties.floor`]: { $lte: addr.floorsTo },
          }),
          ...(addr.floors?.length > 0 && {
            [`address.properties.floor`]: { $in: addr.floors },
          }),
          ...(addr.units?.length > 0 && {
            [`address.properties.unit`]: {
              $in: addr.units.map((u) => new RegExp(`^${u}$`, "i")),
            },
          }),
        },
      ],
    }));

    const qMatch = (search.q || "")
      .split(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/\s]+/)
      .filter((q) => q)
      .map((qPart) => new RegExp(`${qPart}`, "i"))
      .concat([new RegExp(search.q, "i")]);
    console.log("this.request.locale.current", this.request.locale.current);

    return [
      { $match: q },
      {
        $lookup: {
          from: "Blueprint720LayoutGroupings",
          localField: "layoutGroupings",
          foreignField: "code",
          as: "_layoutGroupings",
        },
      },
      ...(search.q
        ? [
            {
              $lookup: {
                from: "Blueprint720Estates",
                localField: "address.properties.estate",
                foreignField: "code",
                as: "address.properties._estate",
              },
            },
            {
              $unwind: {
                path: "$address.properties._estate",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $match: {
                $or: [
                  ...Object.keys(localizeSchema).map((locale) => ({
                    [`name.${locale}`]: { $in: qMatch },
                  })),
                  ...Object.keys(localizeSchema).map((locale) => ({
                    [`ideaDescription.${locale}`]: { $in: qMatch },
                    [`description.${locale}`]: { $in: qMatch },
                  })),
                  { externalId: { $in: qMatch } },
                  { "address.properties.text": { $in: qMatch } },
                  { "address.properties.country": { $in: qMatch } },
                  { "address.properties.state": { $in: qMatch } },
                  { "address.properties.city": { $in: qMatch } },
                  { "address.properties.district": { $in: qMatch } },
                  {
                    "address.properties.administrative_area_1": { $in: qMatch },
                  },
                  { "address.properties.estate": { $in: qMatch } },
                  { "address.properties.building": { $in: qMatch } },
                  ...Object.keys(localizeSchema).map((locale) => ({
                    [`address.properties._estate.name.${locale}`]: {
                      $in: qMatch,
                    },
                  })),
                ],
              },
            },
          ]
        : []),
      ...(addressesMatch.length
        ? [
            {
              $match: {
                $or: addressesMatch,
              },
            },
          ]
        : []),
      {
        $addFields: {
          bookmarkCount: { $size: { $ifNull: ["$bookmarkedUsers", []] } },
          myBookmark: {
            $ifNull: [
              {
                $arrayElemAt: [
                  {
                    $filter: {
                      input: "$bookmarkedUsers",
                      as: "bookmarkedUser",
                      cond: {
                        $eq: ["$$bookmarkedUser.user", currentUser?._id],
                      },
                    },
                  },
                  0,
                ],
              },
              {},
            ],
          },
          isBookmarked: {
            $eq: [
              {
                $size: {
                  $ifNull: [
                    {
                      $setIntersection: [
                        "$bookmarkedUsers.user",
                        [currentUser?._id],
                      ],
                    },
                    [],
                  ],
                },
              },
              1,
            ],
          },
        },
      },
      // if room type count or room space count is included, filter only
      // blueprints with the exact amount of room counts for the specified type
      ...(roomTypeCounts.length || roomSpaceCounts.length
        ? [
            // list out all room space/type counts as new fields
            {
              $addFields: {
                ...roomSpaceCounts.reduce((rObj, r) => {
                  rObj[`rs_${r.type}`] = {
                    $size: {
                      $ifNull: [
                        {
                          $filter: {
                            input: "$rooms",
                            as: "room",
                            cond: {
                              $setIsSubset: [[r.type], "$$room.spaces"],
                            },
                          },
                        },
                        [],
                      ],
                    },
                  };
                  return rObj;
                }, {}),
                ...roomTypeCounts.reduce((rObj, r) => {
                  rObj[`rt_${r.type}`] = {
                    $size: {
                      $ifNull: [
                        {
                          $filter: {
                            input: "$rooms",
                            as: "room",
                            cond: {
                              $setIsSubset: [[r.type], "$$room.types"],
                            },
                          },
                        },
                        [],
                      ],
                    },
                  };
                  return rObj;
                }, {}),
              },
            },
            // check all newly added fields above match our query count
            {
              $match: {
                $and: [
                  ...roomTypeCounts.map((r) => ({
                    [`rt_${r.type}`]: parseInt(r.count.toString(), 10),
                  })),
                  ...roomSpaceCounts.map((r) => ({
                    [`rs_${r.type}`]: parseInt(r.count.toString(), 10),
                  })),
                ],
              },
            },
            // remove our newly added fields for this matching
            {
              $project: {
                ...roomTypeCounts.reduce((rObj, r) => {
                  rObj[`rt_${r.type}`] = 0;
                  return rObj;
                }, {}),
                ...roomSpaceCounts.reduce((rObj, r) => {
                  rObj[`rs_${r.type}`] = 0;
                  return rObj;
                }, {}),
              },
            },
          ]
        : []),
      ...(Object.keys(sort).length ? [{ $sort: sort }] : []),
      ...(search.onlyId
        ? [
            {
              $project: { _id: 1, __v: 1, updatedAt: 1 },
            },
          ]
        : [
            {
              $project: {
                _layoutGroupings: 0,
                bookmarkedUsers: 0,
                "address.properties._estate": 0,
                ...(!search.populates?.includes("data") ? { data: 0 } : {}),
                ...(!search.populates?.includes("versioning")
                  ? { versioning: 0 }
                  : {}),
                ...(!search.populates?.includes("userPermissions")
                  ? { userPermissions: 0 }
                  : {}),
              },
            },
          ]),
    ];
  }

  protected castSort(sortBy: string) {
    const sort: { [key: string]: number } = {};
    const locale = this.request.locale;

    const sortParts = (sortBy || "").match(/(\-?)([\w\d]+)/);

    if (sortParts) {
      const isAscending = sortParts[1] !== "-";
      switch (sortParts[2]) {
        case "recent":
          sort.updatedAt = -1;
          break;
        case "oldest":
          sort.updatedAt = 1;
          break;
        case "name":
          // TODO: localize field
          sort[`name.${locale.current}`] = isAscending ? 1 : -1;
          break;
        case "recentlyBookmarked":
          // sort by recently bookmarked (bookmark place)
          break;
        case "popular":
          // sort by popularity (ie. views)
          break;
        case "sqft":
          sort.sqft = isAscending ? 1 : -1;
          break;
        case "sqftActual":
          sort.sqftActual = isAscending ? 1 : -1;
          break;
        case "recentlySaved":
          sort["myBookmark.time"] = -1;
          break;
        case "oldestSaved":
          sort["myBookmark.time"] = 1;
          break;
        default:
          sort[sortParts[2]] = isAscending ? 1 : -1;
          break;
      }
    } else {
      // default a sort
      sort.updatedAt = 1;
    }

    return sort;
  }

  public async find(
    search: Blueprint720SearchModel,
    paginate: PaginateOptions,
    currentUser?: User
  ) {
    const q = await this.castAggregateQuery(search, currentUser);

    const aggregate = this.blueprint720Repository.aggregate<Blueprint720Doc>(q);

    // already handled in castAggregateQuery
    // const sort = this.castSort(search.sortBy);

    // if (Object.keys(sort).length) {
    //   aggregate.sort(sort);
    // }

    if (search.paginate === undefined || search.paginate) {
      const paginateResult = await this.blueprint720Repository.aggregatePaginate(
        aggregate,
        {
          ...paginate,
          lean: true,
        }
      );

      return paginateResult;
    } else {
      return aggregate;
    }
  }

  public async autocomplete(
    query: AutocompleteSearchModel,
    currentUser?: User
  ) {
    const lang = this.request.locale.current;

    index.setSettings({
      searchableAttributes: [
        "address",
        `name_${lang}`,
        `description_${lang}`,
        `address.properties._estate.name_${lang}`,
        "bookmarkedUsers",
        "externalId",
        "layoutGroupings",
        "isLayout",
        "isNewEstate",
        "isRecommended",
        "rooms",
        "userPermissions",
        "buildingComplexType",
        "shapes",
        "yearBuilt",
        "sqft",
        "sqftActual",
        "styles",
        "assets",
        "createdAt",
        "updatedAt",
      ],
      attributesForFaceting: [
        "filterOnly(owner)",
        "filterOnly(isPublic)",
        "filterOnly(isSystem)",
      ],
      attributeForDistinct: "address.properties.text",
      distinct: 1,
    });

    // filter for isPublic or isSystem or self BP if logged in and me provided
    const filters = `${
      currentUser && query.me !== undefined
        ? (query.me ? "" : "NOT ") + `owner:${currentUser._id} OR `
        : ""
    }isPublic:true OR isSystem:true`;

    const suggestions = await index.search(query.q, {
      filters,
      hitsPerPage: query.limit || 5,
      offset: query.offset,
      page: query.page,
    });

    return {
      recent: [],
      suggestions: suggestions.hits.map((hit) => ({
        searchTerm: (hit as any).address.properties.text,
        text: (hit._highlightResult as any).address.properties.text.value,
        subtext: hit[`name_${lang}`] || "",
        icon: (hit as any).icon,
      })),
    };
  }

  public async findOne(search: Blueprint720SearchModel, currentUser?: User) {
    const q = await this.castAggregateQuery(search, currentUser);
    const aggregate = await this.blueprint720Repository
      .aggregate<Blueprint720>(q)
      .limit(1);

    return aggregate[0] || null;
  }

  public async findSearchHistory(paginate: PaginateOptions, currentUser: User) {
    const viewStats = await this.httpService
      .get(
        `${process.env.API_ANALYTICS}/view-stats/recent-history?${qs.stringify({
          ...paginate,
          types: ["blueprint720"],
          me: true,
        })}`,
        {
          headers: {
            Authorization: this.request.headers.authorization,
          },
        }
      )
      .toPromise();

    const blueprints = viewStats.data._highlightResult?.length
      ? await this.find(
          {
            _ids: viewStats.data._highlightResult?.map((d) => d.typeId),
            fetchAll: true,
            paginate: false,
          },
          {},
          currentUser
        )
      : [];

    return blueprints;
  }

  public async findRecent(paginate: PaginateOptions, currentUser: User) {
    const viewStats = await this.httpService
      .get(
        `${process.env.API_ANALYTICS}/view-stats?${qs.stringify({
          ...paginate,
          types: ["blueprint720"],
          me: true,
        })}`,
        {
          headers: {
            Authorization: this.request.headers.authorization,
          },
        }
      )
      .toPromise();

    const blueprints = await this.find(
      {
        _ids: viewStats.data.docs.map((d) => d.typeId),
        fetchAll: true,
        paginate: false,
      },
      {},
      currentUser
    );

    return {
      // pass back the pagination fields (ie. hasNextPage, offset)
      ...viewStats.data,
      // get blueprints by fetched viewstats' type id
      // (typeId = type item's id)
      docs: viewStats.data.docs
        .map((viewStat) =>
          (blueprints as Blueprint720[]).find(
            (b) => b._id.toHexString() === viewStat.typeId
          )
        )
        .filter((f) => f),
    };
  }

  public async findById(
    _id: string | ObjectId,
    currentUser?: User,
    populates: string[] = []
  ): Promise<Blueprint720> {
    const q = await this.castAggregateQuery(
      { populates, _ids: [typeof _id === "string" ? _id : _id.toHexString()] },
      currentUser
    );
    const aggregate = await this.blueprint720Repository
      .aggregate<Blueprint720>(q)
      .limit(1);
    return aggregate[0] || null;
  }

  public async create(
    createItem: Blueprint720CreateModel,
    currentUser: User,
    files?: { [key: string]: UploadedFile[] },
    populates?: string[]
  ): Promise<Blueprint720> {
    // FIXME: returned url should not need queries
    if (files) await this.uploadFiles(files, createItem);

    const localizedItem = convertFieldsToLocalizeObject<
      Blueprint720CreateModel,
      Blueprint720
    >({
      fields: [
        "name",
        "ideaDescription",
        "description",
        "assets.name",
        "assets.description",
      ],
      obj: createItem,
      locale: this.request.locale.current,
    });

    console.log("LOCALIZED? ", localizedItem);

    if (createItem.name) {
      createItem.name = await this.generateUniqueName(
        localizedItem.name || { _l: "" },
        currentUser
      );
    }

    // If address provided, find a layoutGrouping that has the address criteria
    if (createItem.address) {
      const {
        country,
        state,
        city,
        district,
        administrative_area_1,
        estate,
        building,
        floor,
        unit,
      } = createItem.address.properties;

      const layoutGroupingsMatched = await this.blueprint720LayoutGroupingService.find(
        {
          countries: [country],
          states: [state],
          cities: [city],
          districts: [district],
          administrative_area_1s: [administrative_area_1],
          estates: [estate],
          buildings: [building],
          floors: [floor],
          units: [unit],
          paginate: false,
        },
        {},
        []
      );

      localizedItem.layoutGroupings = (layoutGroupingsMatched as Blueprint720LayoutGrouping[]).map(
        (layoutGrouping) => layoutGrouping._id.toHexString()
      );
    }

    const createdItem = await this.blueprint720Repository.create(localizedItem);

    await this.saveToAlgolia(createdItem);

    return this.findById(createdItem._id.toHexString(), currentUser, populates);
  }

  public async addAssets(
    _id: string,
    newAssets: Asset[],
    files?: UploadedFiles,
    currentUser?: User
  ): Promise<Blueprint720> {
    let blueprint = await this.blueprint720Repository.findOne({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });

    if (!blueprint) {
      throw new NotFoundException("blueprint not found");
    }

    const assetItem = newAssets.map((asset) =>
      convertFieldsToLocalizeObject<Asset, Asset>({
        fields: ["name", "description"],
        obj: asset,
        locale: this.request.locale.current,
      })
    );

    console.log("addAssets assetItem", assetItem);

    blueprint.assets = [...blueprint.assets, ...assetItem];

    // if (files) {
    // goes through files to see what needs to be uploaded
    await this.uploadFiles(files, blueprint);
    // }

    await this.blueprint720Repository.findOneAndUpdate(
      {
        ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
      },
      {
        assets: blueprint.assets,
      },
      { new: true }
    );
    return this.findOne({ _ids: [_id] }, currentUser);
  }

  public async deleteAssets(
    _id: string,
    assetIds: string[],
    currentUser?: User
  ): Promise<Blueprint720> {
    // find blueprint from db
    let blueprint = await this.blueprint720Repository.findOne({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });
    if (!blueprint) {
      throw new NotFoundException("blueprint not found");
    }

    this.isAuthorized(currentUser, blueprint, "delete");

    // Delete all assets associated with deleting assets
    const getDeletingAssets = blueprint.assets.filter(
      (a) => assetIds.includes(a._id.toHexString()) && a.file
    );
    for (const getDeletingAsset of getDeletingAssets) {
      const fileSdk = new FileSDK({
        headerAuthorization: this.request.headers["authorization"],
      });
      await fileSdk.deleteFile(getDeletingAsset.file);
    }

    await this.blueprint720Repository.findOneAndUpdate(
      {
        ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
      },
      {
        assets: blueprint.assets.filter(
          (a) => !assetIds.includes(a._id.toHexString())
        ),
      }
    );
    return this.findOne({ _ids: [_id] }, currentUser);
  }

  public async updateAsset(
    _id: string,
    assetId: string,
    updateAsset: UpdateAssetModel,
    files?: UploadedFiles,
    currentUser?: User
  ): Promise<Blueprint720> {
    // find blueprint from db
    let blueprint: Blueprint720 = (
      await this.blueprint720Repository.findOne({
        ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
      })
    ).toJSON();
    if (!blueprint) {
      throw new NotFoundException("blueprint not found");
    }

    this.isAuthorized(currentUser, blueprint, "edit");

    // find the asset to update
    const assetIndex = blueprint.assets.findIndex(
      (asset) => asset._id.toHexString() === assetId
    );

    // if asset not found, add the asset
    if (assetIndex === -1) {
      return this.addAssets(
        _id,
        [{ ...(updateAsset as Asset), _id: new ObjectId(assetId) }],
        files,
        currentUser
      );
    }

    const assetItem = convertFieldsToLocalizeObject<UpdateAssetModel, Asset>({
      fields: ["name", "description"],
      obj: updateAsset,
      locale: this.request.locale.current,
    });

    console.log("updateAsset assetItem", assetItem);

    const { file, ...otherUpdateFields } = assetItem;

    if (file) {
      if (blueprint.assets[assetIndex].file) {
        // remove prior file
        this.removeAssetFile(blueprint, assetIndex);
      }
      blueprint.assets[assetIndex].file = file;
      // upload file
      if (files) await this.uploadFiles(files, blueprint);
    }

    // update the asset
    blueprint.assets[assetIndex] = {
      ...blueprint.assets[assetIndex],
      ...otherUpdateFields,
    };

    await this.blueprint720Repository.findOneAndUpdate(
      {
        ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
      },
      { assets: blueprint.assets },
      { new: true }
    );
    return this.findOne({ _ids: [_id] }, currentUser);
  }

  public async createBatch(
    createItems: Blueprint720CreateModel[]
  ): Promise<Blueprint720[]> {
    const blueprintItems = createItems.map((item) => {
      const localizedItem = convertFieldsToLocalizeObject<
        Blueprint720CreateModel,
        Blueprint720
      >({
        fields: [
          "name",
          "ideaDescription",
          "description",
          "assets.name",
          "assets.description",
        ],
        obj: item,
        locale: this.request.locale.current,
      });
      return localizedItem;
    });
    // create and return created obj
    return this.blueprint720Repository.insertMany(blueprintItems);
  }

  public async update(
    _id: string,
    updateItem: Blueprint720UpdateModel,
    files?: { [key: string]: UploadedFile[] },
    currentUser?: User,
    populates?: string[]
  ): Promise<Blueprint720> {
    const originalObj: Blueprint720Doc = await this.blueprint720Repository.findOne(
      {
        ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
      }
    );

    this.isAuthorized(currentUser, originalObj, "edit");

    await this.uploadFiles(files, updateItem);

    if (!originalObj) throw new NotFoundException("blueprint not found");

    const localizedItem = convertFieldsToLocalizeObject<
      Blueprint720UpdateModel,
      Blueprint720
    >({
      fields: [
        "name",
        "ideaDescription",
        "description",
        "assets.name",
        "assets.description",
      ],
      obj: updateItem,
      locale: this.request.locale.current,
      originalObj: {
        ...originalObj.toJSON(),
      },
    });

    // If address updated, find a layoutGrouping that has the address criteria
    if (updateItem.address) {
      const {
        country,
        state,
        city,
        district,
        administrative_area_1,
        estate,
        building,
        floor,
        unit,
      } = updateItem.address.properties;

      const layoutGroupingsMatched = await this.blueprint720LayoutGroupingService.find(
        {
          countries: [country],
          states: [state],
          cities: [city],
          districts: [district],
          administrative_area_1s: [administrative_area_1],
          estates: [estate],
          buildings: [building],
          floors: [floor],
          units: [unit],
          paginate: false,
        },
        {},
        []
      );

      localizedItem.layoutGroupings = (layoutGroupingsMatched as Blueprint720LayoutGrouping[]).map(
        (layoutGrouping) => layoutGrouping._id.toHexString()
      );
    }

    if (localizedItem.assets) {
      for (let asset of localizedItem.assets) {
        await this.updateAsset(
          _id,
          asset._id.toString(),
          asset,
          files,
          currentUser
        );
      }
      delete localizedItem.assets;
    }

    const updatedItem = await this.blueprint720Repository.findByIdAndUpdate(
      originalObj._id,
      localizedItem,
      {
        new: true,
      }
    );

    await this.saveToAlgolia(updatedItem);

    return this.findOne({ _ids: [_id], populates }, currentUser);
  }

  public async addLayoutGrouping(
    _ids: Array<string | ObjectId>,
    layoutGrouping: string | ObjectId
  ) {
    const externalsAndIds = this.splitExternalIdsAndIds(
      _ids.map((id) => (typeof id === "string" ? id : id.toHexString()))
    );

    return this.blueprint720Repository.updateMany(
      {
        $or: [
          ...(externalsAndIds._ids.length
            ? [
                {
                  _id: {
                    $in: externalsAndIds._ids.map((id) => new ObjectId(id)),
                  },
                },
              ]
            : []),
          ...(externalsAndIds.externalIds.length
            ? [{ externalId: { $in: externalsAndIds.externalIds } }]
            : []),
        ],
      },
      {
        $push: {
          layoutGroupings:
            typeof layoutGrouping === "string"
              ? layoutGrouping
              : layoutGrouping.toHexString(),
        },
      }
    );
  }

  public async removeLayoutGrouping(
    _ids: Array<string | ObjectId>,
    layoutGrouping: string | ObjectId
  ) {
    const externalsAndIds = this.splitExternalIdsAndIds(
      _ids.map((id) => (typeof id === "string" ? id : id.toHexString()))
    );

    return this.blueprint720Repository.updateMany(
      {
        $or: [
          ...(externalsAndIds._ids.length
            ? [
                {
                  _id: {
                    $in: externalsAndIds._ids.map((id) => new ObjectId(id)),
                  },
                },
              ]
            : []),
          ...(externalsAndIds.externalIds.length
            ? [{ externalId: { $in: externalsAndIds.externalIds } }]
            : []),
        ],
      },
      { $pull: { layoutGroupings: new ObjectId(layoutGrouping) } }
    );
  }

  public async archive(
    _id: string,
    isArchived = true,
    currentUser?: User,
    populates?: string[]
  ): Promise<Blueprint720> {
    // find by id and update isArchived by param
    await this.blueprint720Repository.findOneAndUpdate(
      {
        ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
      },
      { isArchived }
    );

    // archive in Algolia index
    index.partialUpdateObject({ isArchived }).then(({ objectID: _id }) => {
      console.log(_id);
    });

    return this.findOne({ _ids: [_id], populates }, currentUser);
  }

  public async delete(_id: string, currentUser?: User) {
    // find blueprint720 from db
    const blueprint720 = await this.blueprint720Repository.findOne({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });
    // if blueprint720 not found, throw not found error
    if (!blueprint720) {
      throw new NotFoundException("blueprint720 not found");
    }

    this.isAuthorized(currentUser, blueprint720, "delete");

    // delete and return blueprint720
    await this.blueprint720Repository.findOneAndDelete({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });

    // delete from Algolia index
    index.deleteObject(_id);

    return this.findOne({ _ids: [_id] }, currentUser);
  }

  public async deleteMultiple(_ids: string[]) {
    const externalsAndIds = this.splitExternalIdsAndIds(_ids);
    // get blueprint720 documents from db
    const blueprint720List = await this.blueprint720Repository.find({
      $or: [
        ...(externalsAndIds._ids.length
          ? [
              {
                _id: {
                  $in: externalsAndIds._ids.map((id) => new ObjectId(id)),
                },
              },
            ]
          : []),
        ...(externalsAndIds.externalIds.length
          ? [{ externalId: { $in: externalsAndIds.externalIds } }]
          : []),
      ],
    });
    // if blueprint720List length not match, throw error
    if (blueprint720List.length !== _ids.length) {
      const foundIds = blueprint720List.map((f) => f._id.toHexString());
      throw new NotFoundException(
        `the following blueprint720 were not found: ${_ids.filter(
          (id) => !foundIds.includes(id)
        )}`
      );
    }
    // remove from db
    await this.blueprint720Repository.deleteMany({
      _id: { $in: blueprint720List.map((f) => f._id) },
    });
    // return removed blueprint720List
    return blueprint720List;
  }

  public async addUserPermission(
    _id: string,
    newPermission: UserPermissions,
    currentUser?: User
  ): Promise<Blueprint720> {
    // find blueprint from db
    const blueprint = await this.blueprint720Repository.findOne(
      isValidObjectId(_id) ? { _id } : { externalId: _id }
    );

    if (!blueprint) {
      throw new NotFoundException("blueprint not found");
    }

    await this.blueprint720Repository.findOneAndUpdate(
      isValidObjectId(_id) ? { _id } : { externalId: _id },
      {
        userPermissions: [newPermission, ...blueprint.userPermissions],
      }
    );

    return this.findOne(
      { _ids: [blueprint._id.toHexString()], paginate: false },
      currentUser
    );
  }

  public async editUserPermission(
    blueprint: string | Blueprint720,
    permissionId: string,
    editPermission: EditUserPermissionModel,
    currentUser?: User
  ): Promise<Blueprint720> {
    // find blueprint from db
    const blueprintObj =
      typeof blueprint === "string"
        ? await this.blueprint720Repository.findOne({
            ...(isValidObjectId(blueprint)
              ? { _id: blueprint }
              : { externalId: blueprint }),
          })
        : blueprint;

    if (!blueprintObj) {
      throw new NotFoundException("blueprint not found");
    }

    this.isAuthorized(currentUser, blueprintObj, "edit");

    if (!editPermission["read"]) {
      editPermission["edit"] = false;
      editPermission["delete"] = false;
    }

    // find the user permission
    const userPermission = blueprintObj.userPermissions.find((up) => {
      return up._id.equals(permissionId);
    });

    if (!userPermission) {
      throw new NotFoundException("user permission not found in this file");
    }

    Object.keys(editPermission).forEach((editPermissionKey) => {
      userPermission[editPermissionKey] = editPermission[editPermissionKey];
    });

    await this.blueprint720Repository.findByIdAndUpdate(
      blueprintObj._id,
      {
        userPermissions: blueprintObj.userPermissions,
      },
      { timestamps: false }
    );

    return this.findById(blueprintObj._id, currentUser);
  }

  public async removeUserPermission(
    _id: string,
    permissionId: string,
    currentUser?: User
  ): Promise<Blueprint720> {
    if (!permissionId) {
      throw new NotFoundException("no such permission found");
    }

    // find blueprint from db
    const blueprint = await this.blueprint720Repository.findOne({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });
    if (!blueprint) {
      throw new NotFoundException("blueprint not found");
    }

    this.isAuthorized(currentUser, blueprint, "delete");

    await this.blueprint720Repository.findOneAndUpdate(
      isValidObjectId(_id) ? { _id } : { externalId: _id },
      {
        userPermissions: blueprint.userPermissions.filter(
          (up) => up._id.equals(permissionId) === false
        ),
      },
      { timestamps: false }
    );
    return this.findOne(
      { _ids: [blueprint._id.toHexString()], paginate: false },
      currentUser
    );
  }

  public async publishVersion(
    _id: string,
    currentUser?: User
  ): Promise<Blueprint720> {
    // find blueprint from db
    const blueprint = await this.blueprint720Repository.findOne({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });
    if (!blueprint) {
      throw new NotFoundException("blueprint not found");
    }

    this.isAuthorized(currentUser, blueprint, "edit");

    // compare current data with the auto-saved data
    const delta: Object = jsondiffpatch.diff(
      blueprint.versioning.versionData || {},
      blueprint.data
    );

    // add 1 to the new version
    const newVersion = (blueprint.versioning.version || 0) + 1;

    await this.blueprint720Repository.findOneAndUpdate(
      isValidObjectId(_id) ? { _id } : { externalId: _id },
      {
        versioning: {
          // add this new version to the array of versions
          versions: [
            { no: newVersion, delta },
            ...blueprint.versioning.versions,
          ],
          // the auto-saved data becomes the current data
          versionData: blueprint.data,
          // update the version
          version: newVersion,
        },
      }
    );

    return this.findOne({ _ids: [_id], paginate: false }, currentUser);
  }

  // find data of a version in blueprint
  public async getDataByVersion(
    blueprint: string | Blueprint720,
    version: number
  ): Promise<Data> {
    // find blueprint from db
    const blueprintObj =
      typeof blueprint === "string"
        ? await this.blueprint720Repository.findOne({
            ...(isValidObjectId(blueprint)
              ? { _id: blueprint }
              : { externalId: blueprint }),
          })
        : blueprint;

    if (!blueprintObj) {
      throw new NotFoundException("blueprint not found");
    }

    let data = blueprintObj.versioning.versionData as Data;

    for (const savedVersion of blueprintObj.versioning.versions.sort(
      (a, b) => b.no - a.no // version number from highest to smallest
    )) {
      if (savedVersion.no === version) return data;

      // the queried version is smaller than latest version
      data = jsondiffpatch.unpatch(data, savedVersion.delta);
    }
  }

  public async uploadFiles(
    files: { [key: string]: UploadedFile[] } | undefined,
    baseObj: Blueprint720UploadedFiles,
    options?: {
      // used for fetching existing asset document. This allows
      // us to delete existing files that will be replaced
      updateId?: string;
      isArchived?: boolean;
      organization?: string;
    }
  ) {
    // define options
    const opts = { ...options };
    // get the header authorization
    const headerAuthorization = this.request.headers["authorization"];
    // instantiate fileSdk
    const fileSdk = await new FileSDK({
      headerAuthorization,
    });

    // if asset files are passed in, map form fields back to files
    if (files?.assetFiles?.length || baseObj.assets?.length) {
      const uploadAssetFile = (files?.assetFiles || []).reduce(
        (fileObj, file) => {
          fileObj[file.originalname] = {
            file,
            uploadedFile: null,
          };
          return fileObj;
        },
        {}
      );
      for (let [assetIndex, asset] of baseObj.assets.entries()) {
        if (!asset.file) continue;
        // if file is a file name, it is meant for upload
        if (/[\d\w\s_-]+\.[\d\w]+/.test(asset.file)) {
          const fileName = asset.file;
          if (!uploadAssetFile[fileName])
            throw new Error(`cannot find filename "${asset.file}"`);
          if (!uploadAssetFile[fileName].uploadedFile) {
            uploadAssetFile[
              fileName
            ].uploadedFile = await fileSdk.uploadFileFromStream(
              uploadAssetFile[fileName].file.buffer,
              uploadAssetFile[fileName].file.size,
              {
                qualities: [0.75, 0.5, 0.25],
                blobName: uploadAssetFile[fileName].file.originalname,
                mimeType: uploadAssetFile[fileName].file.mimetype,
                isArchived: opts.isArchived,
                organization: opts.organization,
                bucketFilePath: `${process.env.FILE_SDK_BUCKET_FILE_PATH}-assets`,
              }
            );
          }
          const origFile =
            (baseObj.assets[assetIndex] as any)?.toJSON?.() ||
            baseObj.assets[assetIndex];
          const assetsFile = uploadAssetFile[fileName].uploadedFile;
          baseObj.assets[assetIndex] = {
            ...origFile,
            file: assetsFile._id,
            ...(assetsFile.compressions.length > 1
              ? {
                  url: assetsFile.compressions.find((c) => c.quality === 1)
                    ?.url,
                  large: assetsFile.compressions.find((c) => c.quality === 0.75)
                    ?.url,
                  medium: assetsFile.compressions.find((c) => c.quality === 0.5)
                    ?.url,
                  small: assetsFile.compressions.find((c) => c.quality === 0.25)
                    ?.url,
                }
              : {
                  url: assetsFile?.url,
                }),
          };
        } else if (asset.file.length > 50) {
          // regular objectId length is 24
          // this is probably a base64, transform it to stream and upload it
          // FIXME: temp handling for base64
          const imgBuffer = Buffer.from(asset.file, "base64");
          const assetsFile = await fileSdk.uploadFileFromStream(
            imgBuffer,
            imgBuffer.byteLength,
            {
              qualities: [0.75, 0.5, 0.25],
              // blobName: uploadAssetFile[fileName].file.originalname,
              mimeType: "image/png",
              isArchived: opts.isArchived,
              organization: opts.organization,
              bucketFilePath: `${process.env.FILE_SDK_BUCKET_FILE_PATH}-assets`,
            }
          );
          if (!assetsFile._id) {
            throw new Error(
              `cannot convert asset at index ${assetIndex} to base64`
            );
          }
          // uploadAssetFile[fileName].hasUploaded = true;
          const origFile =
            (baseObj.assets[assetIndex] as any)?.toJSON?.() ||
            baseObj.assets[assetIndex];
          baseObj.assets[assetIndex] = {
            ...origFile,
            file: assetsFile._id,
            ...(assetsFile.compressions.length > 1
              ? {
                  url: assetsFile.compressions.find((c) => c.quality === 1).url,
                  large: assetsFile.compressions.find((c) => c.quality === 0.75)
                    .url,
                  medium: assetsFile.compressions.find((c) => c.quality === 0.5)
                    .url,
                  small: assetsFile.compressions.find((c) => c.quality === 0.25)
                    .url,
                }
              : {
                  url: assetsFile.url,
                }),
          };
        }
      }
    }
    // return updated baseObj
    return baseObj;
  }

  protected async uploadAssetFile(blueprint: Blueprint720, assetIndex: number) {
    const fileSdk = new FileSDK({
      headerAuthorization: this.request.headers["authorization"],
    });
    fileSdk.uploadFileFromFilePath(blueprint.assets[assetIndex].file);
  }

  protected async removeAssetFile(blueprint: Blueprint720, assetIndex: number) {
    const fileSdk = new FileSDK({
      headerAuthorization: this.request.headers["authorization"],
    });
    fileSdk.deleteFile(blueprint.assets[assetIndex].file);
  }

  public async addBookmark(
    _id: string,
    currentUser?: User
  ): Promise<Blueprint720> {
    const blueprintToAddBookmark = await this.blueprint720Repository.findOneAndUpdate(
      isValidObjectId(_id) ? { _id } : { externalId: _id },
      {
        $addToSet: {
          bookmarkedUsers: {
            user: new ObjectId(currentUser._id),
            time: new Date(),
          },
        },
      },
      { timestamps: false }
    );

    // update record in Algolia index
    index
      .partialUpdateObject({
        bookmarkedUsers: blueprintToAddBookmark.bookmarkedUsers,
      })
      .then(({ objectID: _id }) => {
        console.log(_id);
      });

    return this.findOne({ _ids: [_id], paginate: false }, currentUser);
  }

  public async removeBookmark(_id: string, currentUser?: User) {
    const blueprintToRemoveBookmark = await this.blueprint720Repository.findOneAndUpdate(
      isValidObjectId(_id) ? { _id } : { externalId: _id },
      {
        $pull: {
          bookmarkedUsers: {
            user: new ObjectId(currentUser._id),
          },
        },
      },
      { timestamps: false }
    );

    // update record in Algolia index
    index
      .partialUpdateObject({
        bookmarkedUsers: blueprintToRemoveBookmark.bookmarkedUsers,
      })
      .then(({ objectID: _id }) => {
        console.log(_id);
      });

    return this.findOne({ _ids: [_id], paginate: false }, currentUser);
  }

  public async shakeData(
    _id: string | Blueprint720,
    filters: Blueprint720ShakeDataFiltersModel,
    currentUser: User
  ): Promise<Data> {
    const blueprintData =
      filters.version !== undefined
        ? await this.getDataByVersion(_id, filters.version)
        : (typeof _id === "string"
            ? await this.blueprint720Repository.findOne({
                ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
              })
            : _id
          )?.data;

    if (!blueprintData) {
      throw new NotFoundException("blueprint not found");
    }

    const resultJson = { ...blueprintData };

    if (filters.rooms?.length) {
      resultJson.floorplan = [];
      resultJson.Save_Furniture = [];
      for (const floorplan of blueprintData.floorplan) {
        if ((filters.rooms || []).includes(floorplan._id)) {
          resultJson.floorplan.push(floorplan);
        }
      }
    }

    switch (filters.type) {
      case "layout":
        resultJson.Save_Furniture = [];
        // TODO: change all wall/floor/ceiling to one material
        resultJson.floorplan.forEach((fp) => {
          fp.floormat = {
            ...fp.floormat,
            MaterialID: "0",
            MaterialColor: { R: 1, G: 1, B: 1, A: 1 },
          };
          fp.CeillingMat = {
            ...fp.CeillingMat,
            MaterialID: "2",
            MaterialColor: { R: 1, G: 1, B: 1, A: 1 },
          };
          fp.walls.forEach((w) => {
            w.frontMat = {
              ...w.frontMat,
              MaterialID: "2",
              MaterialColor: { R: 1, G: 1, B: 1, A: 1 },
            };
            w.backMat = {
              ...w.backMat,
              MaterialID: "2",
              MaterialColor: { R: 1, G: 1, B: 1, A: 1 },
            };
            w.frontSkirtMat = {
              ...w.frontSkirtMat,
              MaterialID: "1",
              MaterialColor: { R: 1, G: 1, B: 1, A: 1 },
            };
            w.backSkirtMat = {
              ...w.backSkirtMat,
              MaterialID: "1",
              MaterialColor: { R: 1, G: 1, B: 1, A: 1 },
            };
          });
        });
        break;
      case "full":
      default:
        break;
    }

    return resultJson;
  }

  public async cloneBlueprint(
    _id: string,
    filters: Blueprint720ShakeDataFiltersModel,
    currentUser: User,
    options?: { save?: boolean }
  ) {
    const blueprint = await this.blueprint720Repository.findOne({
      ...(isValidObjectId(_id) ? { _id } : { externalId: _id }),
    });

    if (!blueprint) throw new NotFoundException("blueprint not found");

    const clonedBlueprint = blueprint.toJSON();

    if (clonedBlueprint.name) {
      clonedBlueprint.name = await this.generateUniqueName(
        clonedBlueprint.name,
        currentUser
      );
    }
    clonedBlueprint.owner = currentUser._id;
    clonedBlueprint.clonedFrom = blueprint._id;
    clonedBlueprint.versioning = {
      version: 0,
      versionData: {},
      versions: [],
    };
    clonedBlueprint.isSystem = false;
    clonedBlueprint.externalId = "";
    clonedBlueprint.isPublic = false;
    clonedBlueprint.data = await this.shakeData(
      blueprint,
      filters,
      currentUser
    );
    clonedBlueprint._id = new ObjectId();
    clonedBlueprint.assets = (clonedBlueprint.assets || []).filter(
      (a) => a.type === "layout"
    );

    if (options?.save) {
      return this.blueprint720Repository.create(clonedBlueprint);
    }

    return clonedBlueprint;
  }
}
