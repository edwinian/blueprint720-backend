import { Transform } from "class-transformer";
import { IsString, IsOptional, IsNumber } from "class-validator";
import transformNumber from "src/core/classTransformers/transformNumber";
export class Blueprint720ShakeDataFiltersModel {
  @IsOptional()
  @IsString({ each: true })
  rooms?: string[];

  @IsOptional()
  @IsNumber()
  @Transform(transformNumber)
  version?: number;

  @IsOptional()
  @IsString()
  type?: "full" | "layout";
}
