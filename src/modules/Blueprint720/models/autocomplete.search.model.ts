import { Transform } from "class-transformer";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";
import transformBoolean from "src/core/classTransformers/transformBoolean";

export class AutocompleteSearchModel {
  @IsOptional()
  @IsString()
  q?: string;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  me?: string | boolean;

  @IsOptional()
  @IsNumber()
  limit?: number;

  @IsOptional()
  @IsNumber()
  offset?: number;

  @IsOptional()
  @IsNumber()
  page?: number;
}
