import { IsString, IsOptional } from "class-validator";
export class Blueprint720RoomModel {
  @IsOptional()
  @IsString()
  roomId: string;

  @IsOptional()
  @IsString({ each: true })
  types: string[];

  @IsOptional()
  @IsString({ each: true })
  spaces: string[];
}
