import { Type } from "class-transformer";
import {
  IsString,
  IsOptional,
  ValidateNested,
  IsBoolean,
  IsNumber,
  IsArray,
} from "class-validator";
import { LocalizeModel } from "../../../core/mongo/localize.model";
import {
  Address,
  Asset,
  Data,
  UserPermissions,
} from "../interfaces/blueprint720";
import { Blueprint720RoomModel } from "./blueprint720.room.model";
export class Blueprint720CreateDTOModel {
  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  name: string | LocalizeModel;

  @IsOptional()
  @IsString({ each: true })
  layoutGroupings?: string[];

  @IsOptional()
  @ValidateNested()
  data?: Data;

  @IsOptional()
  @ValidateNested({ each: true })
  @IsArray()
  userPermissions?: UserPermissions[];

  @IsOptional()
  @ValidateNested()
  address?: Address;

  @IsString()
  @IsOptional()
  buildingComplexType?: string;

  @IsString({ each: true })
  @IsOptional()
  shapes?: string[];

  @ValidateNested({ each: true })
  assets: Asset[];

  @IsOptional()
  @IsNumber()
  yearBuilt?: number;

  @IsOptional()
  @IsNumber()
  sqft?: number;

  @IsOptional()
  @IsArray()
  styles: string[];

  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  ideaDescription?: string | LocalizeModel;

  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  description?: string | LocalizeModel;

  @IsOptional()
  @IsBoolean()
  isArchived?: boolean;

  @IsOptional()
  @IsBoolean()
  isPublic?: boolean;

  @IsOptional()
  @ValidateNested({ each: true })
  rooms?: Blueprint720RoomModel[];
}
