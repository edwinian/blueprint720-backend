import { IsString, IsOptional } from "class-validator";

export class Blueprint720MetaModel {
  @IsString()
  @IsOptional()
  layout?: string;

  @IsString()
  @IsOptional()
  ideaDescription?: string;

  @IsString()
  @IsOptional()
  description?: string;

  @IsString({ each: true })
  @IsOptional()
  styles?: string[];
}
