import {
  IsString,
  IsBoolean,
  IsOptional,
  ValidateNested,
  IsNumber,
} from "class-validator";
import { Transform } from "class-transformer";
import { BaseSearchModel } from "../../../core/mongo/BaseSearchModel";
import { UserPermissions } from "../interfaces/blueprint720";
import transformBoolean from "src/core/classTransformers/transformBoolean";
import transformNumber from "src/core/classTransformers/transformNumber";

class AddressQuery {
  @IsOptional()
  @IsString({ each: true })
  countries?: string[];
  @IsOptional()
  @IsString({ each: true })
  states?: string[];
  @IsOptional()
  @IsString({ each: true })
  cities?: string[];
  @IsOptional()
  @IsString({ each: true })
  districts?: string[];
  @IsOptional()
  @IsString({ each: true })
  administrative_area_1s?: string[];
  @IsOptional()
  @IsString({ each: true })
  estates?: string[];
  @IsOptional()
  @IsString({ each: true })
  buildings?: string[];
  @IsOptional()
  @IsNumber()
  floorsFr?: number;
  @IsOptional()
  @IsNumber()
  floorsTo?: number;
  @IsOptional()
  @Transform(transformNumber)
  @IsNumber({}, { each: true })
  floors?: number[];
  @IsOptional()
  @IsString({ each: true })
  units?: string[];
}

class Blueprint720SearchRoomCountModel {
  @IsString()
  type: string;

  @IsOptional()
  @Transform(transformNumber)
  @IsNumber()
  count?: number;
}

export class Blueprint720SearchModel extends BaseSearchModel {
  // name, ideaDescription, description, address queried by q in BaseSearchModel

  @IsString({ each: true })
  @IsOptional()
  regions?: string[];

  @IsString({ each: true })
  @IsOptional()
  shapes?: string[];

  @IsString({ each: true })
  @IsOptional()
  assetTypes?: string[];

  @IsOptional()
  @IsString({ each: true })
  layoutGroupings?: string[];

  @IsOptional()
  @IsString({ each: true })
  layoutGroupingsNotEqual?: string[];

  @IsOptional()
  roomTypes?: Array<{ type: string; count: number }>;

  @IsOptional()
  @ValidateNested({ each: true })
  roomSpaces?: Blueprint720SearchRoomCountModel[];

  @IsOptional()
  @IsString({ each: true })
  styles?: string[];

  @IsString()
  @IsOptional()
  owner?: string;

  @IsOptional()
  @ValidateNested({ each: true })
  userPermissions?: UserPermissions[];

  // query address properties except for text, floor, unit, and elevation_m
  @IsOptional()
  @ValidateNested({ each: true })
  addresses?: AddressQuery[];

  @IsString({ each: true })
  @IsOptional()
  buildingComplexTypes?: string[];

  @IsOptional()
  @IsNumber()
  yearBuilt?: number;

  @IsOptional()
  @IsNumber()
  yearBuiltMin?: number;

  @IsOptional()
  @IsNumber()
  yearBuiltMax?: number;

  @IsOptional()
  @IsNumber()
  sqftActual?: number;

  @IsOptional()
  @IsNumber()
  sqft?: number;

  @IsOptional()
  @IsNumber()
  sqftMin?: number;

  @IsOptional()
  @IsNumber()
  sqftMax?: number;

  @IsOptional()
  @IsNumber()
  sqftActualMin?: number;

  @IsOptional()
  @IsNumber()
  sqftActualMax?: number;

  @IsString()
  @IsOptional()
  ideaDescription?: string;

  @IsString()
  @IsOptional()
  description?: string;

  @IsBoolean()
  @Transform(transformBoolean)
  @IsOptional()
  isArchived?: string | boolean;

  @IsBoolean()
  @Transform(transformBoolean)
  @IsOptional()
  isPublic?: string | boolean;

  @IsBoolean()
  @Transform(transformBoolean)
  @IsOptional()
  isLayout?: string | boolean;

  @IsBoolean()
  @Transform(transformBoolean)
  @IsOptional()
  isSystem?: string | boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  isNewEstate?: string | boolean;

  @IsBoolean()
  @Transform(transformBoolean)
  @IsOptional()
  isRecommended?: string | boolean;

  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  me?: string | boolean;

  @IsString()
  @IsOptional()
  sortBy?: "recent" | "oldest";

  @IsBoolean()
  @Transform(transformBoolean)
  @IsOptional()
  isBookmarked?: string | boolean;

  @IsString({ each: true })
  @IsOptional()
  bookmarkedBy?: string[];

  // FIXME: THIS IS FOR ADMIN USE, NEED MORE HANDLING
  @IsOptional()
  @Transform(transformBoolean)
  @IsBoolean()
  fetchAll?: string | boolean;

  @IsOptional()
  @IsString({ each: true })
  externalIds?: string[];
}
