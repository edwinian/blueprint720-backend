import { IsString } from "class-validator";

export class Blueprint720PreviewModel {
  @IsString()
  file: string;
  @IsString()
  url: string;
  @IsString()
  thumbnailUrl: string;
}
