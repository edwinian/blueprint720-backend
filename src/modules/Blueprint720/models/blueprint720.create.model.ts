import { Type } from "class-transformer";
import {
  IsString,
  IsOptional,
  ValidateNested,
  IsBoolean,
  IsNumber,
  IsArray,
} from "class-validator";
import { LocalizeModel } from "../../../core/mongo/localize.model";
import {
  UserPermissions,
  Address,
  Asset,
  Versioning,
  Data,
} from "../interfaces/blueprint720";
import { Blueprint720RoomModel } from "./blueprint720.room.model";
export class Blueprint720CreateModel {
  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  @ValidateNested()
  name: string | LocalizeModel;

  @IsOptional()
  @IsString({ each: true })
  layoutGroupings?: string[];

  @IsOptional()
  @ValidateNested()
  versioning: Versioning;

  @IsOptional()
  @ValidateNested()
  data?: Data;

  @IsString()
  owner: string;

  @ValidateNested({ each: true })
  rooms: Blueprint720RoomModel[];

  @IsOptional()
  @ValidateNested({ each: true })
  @IsArray()
  userPermissions?: UserPermissions[];

  @IsOptional()
  @ValidateNested()
  address?: Address;

  @IsString()
  @IsOptional()
  buildingComplexType?: string;

  @IsString({ each: true })
  @IsOptional()
  shapes?: string[];

  @ValidateNested({ each: true })
  assets: Asset[];

  @IsOptional()
  @IsNumber()
  yearBuilt?: number;

  @IsOptional()
  @IsNumber()
  sqft?: number;

  @IsOptional()
  @IsNumber()
  sqftActual?: number;

  @IsOptional()
  @ValidateNested({ each: true })
  @IsArray()
  styles?: string[];

  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  ideaDescription?: string | LocalizeModel;

  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  description?: string | LocalizeModel;

  @IsBoolean()
  @IsOptional()
  isArchived?: boolean;

  @IsBoolean()
  @IsOptional()
  isPublic?: boolean;

  @IsBoolean()
  @IsOptional()
  isSystem?: boolean;
}
