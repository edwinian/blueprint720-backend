import { Type } from "class-transformer";
import {
  IsString,
  IsOptional,
  IsBoolean,
  ValidateNested,
  IsEnum,
} from "class-validator";
import { LocalizeString } from "src/core/mongo/localize";
import { LocalizeModel } from "../../../core/mongo/localize.model";
import { Address, Asset, Data, Versioning } from "../interfaces/blueprint720";
import { Blueprint720RoomModel } from "./blueprint720.room.model";

export class Blueprint720UpdateModel {
  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  name?: string | LocalizeModel;

  @IsString()
  @IsOptional({ each: true })
  layoutGroupings?: string[];

  @IsOptional()
  @ValidateNested()
  versioning?: Versioning;

  @IsOptional()
  @ValidateNested()
  data?: Data;

  @IsOptional()
  address?: Address;

  @IsOptional()
  @ValidateNested({ each: true })
  rooms?: Blueprint720RoomModel[];

  @IsString()
  @IsOptional()
  buildingComplexType?: string;

  @IsString({ each: true })
  @IsOptional()
  shapes?: string[];

  @ValidateNested({ each: true })
  @IsOptional()
  assets?: Asset[];

  @IsOptional()
  yearBuilt?: number;

  @IsOptional()
  sqft?: number;

  @IsString({ each: true })
  @IsOptional()
  styles?: string[];

  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  ideaDescription?: string | LocalizeModel;

  @IsOptional()
  @Type((a) => (typeof a === "string" ? String : LocalizeModel))
  description?: string | LocalizeModel;

  @IsBoolean()
  @IsOptional()
  isArchived?: boolean;

  @IsBoolean()
  @IsOptional()
  isPublic?: boolean;

  @IsBoolean()
  @IsOptional()
  isSystem?: boolean;
}

export class EditUserPermissionModel {
  @IsOptional()
  @IsBoolean()
  read?: boolean;
  @IsOptional()
  @IsBoolean()
  edit?: boolean;
  @IsOptional()
  @IsBoolean()
  delete?: boolean;
}

export class UpdateAssetModel {
  @IsOptional()
  @IsString()
  @IsEnum(["render", "layout", "360", "video"])
  type?: string;
  @IsOptional()
  @IsString()
  file?: string;
  @IsOptional()
  name?: LocalizeString | string;
  @IsOptional()
  description?: LocalizeString | string;
  @IsOptional()
  position360?: { x: number; y: number; z: number };
}
