import { Schema, SchemaTypes } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import mongooseAggregatePaginate from "mongoose-aggregate-paginate-v2";
import bookmarkedUserSchema from "../../../core/mongo/bookmarkedUser.schema";
import addressSchema from "../../../core/mongo/address.schema";
import localizeSchema from "../../../core/mongo/localize.schema";
import assetSchema from "../../../core/mongo/asset.schema";
// Keep mongoose schemas synced with Algolia
import mongoolia from "mongoolia";

export const Blueprint720Schema = new Schema(
  // Pass `{algoliaIndex: true}` to push theses attributes for indexing to Algolia
  {
    name: { type: localizeSchema, algoliaIndex: true },
    bookmarkedUsers: {
      type: [bookmarkedUserSchema],
      select: false,
      default: [],
      algoliaIndex: true,
    },
    externalId: { type: String, sparse: true, algoliaIndex: true },
    layoutGroupings: [
      {
        type: SchemaTypes.ObjectId,
        sparse: true,
        algoliaIndex: true,
      },
    ],
    isLayout: { type: Boolean, index: true, algoliaIndex: true },
    isNewEstate: { type: Boolean, index: true, algoliaIndex: true },
    isRecommended: { type: Boolean, index: true, algoliaIndex: true },
    clonedFrom: {
      type: SchemaTypes.ObjectId,
      sparse: true,
      ref: "Blueprint720s",
    },
    versioning: {
      type: {
        version: { type: Number, index: true },
        versionData: SchemaTypes.Mixed,
        versions: [
          {
            no: Number,
            delta: SchemaTypes.Mixed,
          },
        ],
      },
    },
    data: { type: SchemaTypes.Mixed, default: {} },
    rooms: {
      type: [
        {
          roomId: { type: String, index: true },
          types: [String],
          spaces: [String],
        },
      ],
      algoliaIndex: true,
    },
    owner: { type: SchemaTypes.ObjectId, index: true, algoliaIndex: true },
    userPermissions: {
      type: [
        {
          user: { type: SchemaTypes.ObjectId },
          read: { type: Boolean, default: true },
          edit: Boolean,
          delete: Boolean,
        },
      ],
      algoliaIndex: true,
    },
    address: addressSchema,
    buildingComplexType: { type: String, index: true, algoliaIndex: true },
    assets: {
      type: [assetSchema],
    },
    shapes: { type: [String], algoliaIndex: true },
    yearBuilt: { type: Number, sparse: true, algoliaIndex: true },
    sqft: { type: Number, sparse: true, algoliaIndex: true },
    sqftActual: { type: Number, sparse: true, algoliaIndex: true },
    styles: [{ type: String, algoliaIndex: true }],
    ideaDescription: { type: localizeSchema },
    description: { type: localizeSchema },
    isArchived: { type: Boolean, sparse: true, algoliaIndex: true },
    isPublic: { type: Boolean, sparse: true, algoliaIndex: true },
    isSystem: {
      type: Boolean,
      default: false,
      index: true,
      algoliaIndex: true,
    },
    __v: { type: Number, default: 0 },
  },
  {
    collection: "Blueprint720s",
    timestamps: true,
  }
);

const updateVersion = function () {
  const update = this.getUpdate();
  if (update.__v != null) {
    delete update.__v;
  }
  const keys = ["$set", "$setOnInsert"];
  for (const key of keys) {
    if (update[key] != null && update[key].__v != null) {
      delete update[key].__v;
      if (Object.keys(update[key]).length === 0) {
        delete update[key];
      }
    }
  }
  update.$inc = update.$inc || {};
  update.$inc.__v = 1;
};

Blueprint720Schema.pre("update", updateVersion);
Blueprint720Schema.pre("findOneAndUpdate", updateVersion);

Blueprint720Schema.index({ "address.geometry": "2dsphere" });

Blueprint720Schema.plugin(mongoosePaginate);
Blueprint720Schema.plugin(mongooseAggregatePaginate);
// Blueprint720Schema.plugin(mongoolia, {
//   appId: process.env.ALGOLIA_APPLICATION_ID,
//   apiKey: process.env.ALGOLIA_ADMIN_KEY,
//   indexName: `${process.env.NODE_ENV}_blueprint720`,
// });
