import seed1 from "./seeds/seed1";
import seed2 from "./seeds/seed2";
import seed3 from "./seeds/seed3";
import seed4 from "./seeds/seed4";
import seed5 from "./seeds/seed5";

export default {
  model: "Blueprint720s",
  documents: [seed1, seed2, seed3, seed4, seed5],
};
