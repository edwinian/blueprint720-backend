import {
  Get,
  Put,
  Body,
  Post,
  Param,
  Query,
  Delete,
  UseGuards,
  Controller,
  UseInterceptors,
  UploadedFiles,
  NotFoundException,
} from "@nestjs/common";
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import { AuthGuard } from "../Auth/user.auth.guard";
import extractPaginate from "../../core/mongo/extractPaginate";

// services
import { Blueprint720Service } from "./blueprint720.service";

// models
import {
  Blueprint720UpdateModel,
  Blueprint720SearchModel,
  EditUserPermissionModel,
  UpdateAssetModel,
} from "./models";
import { Asset, UserPermissions } from "./interfaces/blueprint720";
import { CurrentUser } from "../../core/decorators/currentUser.decorator";
import { Blueprint720CreateDTOModel } from "./models/blueprint720.create.dto.model";
import { User } from "../Auth/interfaces/user";
import { ParseFormDataInterceptor } from "../../core/interceptors/ParseFormDataInterceptor";
import { fileFilterMultiple } from "../../core/pipes/fileFilterMultiple";
import { UploadedFile } from "@oodles-dev/file-sdk/build/main/lib/file-sdk";
import { Blueprint720ShakeDataFiltersModel } from "./models/blueprint720.shakeData.filters.model";
import { AutocompleteSearchModel } from "./models/autocomplete.search.model";
import { localeInterceptorFactory } from "@oodles-dev/nest-locale-module";

const LocaleInterceptor = localeInterceptorFactory(
  [
    "name",
    "description",
    "ideaDescription",
    "assets.name",
    "assets.description",
  ],
  { exclude: ["data"] }
);

@Controller("blueprint720s")
@UseGuards(AuthGuard({ optional: true }))
export class Blueprint720Controller {
  constructor(private readonly blueprint720Service: Blueprint720Service) {}

  // Routes
  // GET /blueprint720s
  // GET /blueprint720s/search-history
  // GET /blueprint720s/auto-complete
  // GET /blueprint720s/find-one
  // GET /blueprint720s/:_id
  // POST /blueprint720s
  // PUT /blueprint720s/:_id
  // PUT /blueprint720s/:_id/archive/
  // DELETE /blueprint720s/:_id

  // POST /blueprint720s/:_id/user-permissions/
  // PUT /blueprint720s/:_id/user-permissions/:userId
  // DELETE /blueprint720s/:_id/user-permissions/:userId

  // POST /blueprint720s/:_id/assets
  // PUT /blueprint720s/:_id/assets?assetIds[]
  // DELETE /blueprint720s/:_id/assets/:_id

  // PUT /blueprint720s/:_id/publish-version
  // GET /blueprint720s/:_id/get-data-by-version

  // POST /blueprint720s/:_id/bookmarks/:userId
  // DELETE /blueprint720s/:_id/bookmarks/:userId

  @Get()
  @UseInterceptors(LocaleInterceptor)
  public async find(
    @Query() query: Blueprint720SearchModel,
    @CurrentUser() currentUser?: User
  ) {
    const blueprint720 = await this.blueprint720Service.find(
      query,
      extractPaginate(query),
      currentUser
    );
    return blueprint720;
  }

  @Get("auto-complete")
  @UseInterceptors(LocaleInterceptor)
  @UseGuards(AuthGuard({ optional: true }))
  public async autocomplete(
    @Query() query: AutocompleteSearchModel,
    @CurrentUser() currentUser: User
  ) {
    const blueprint720 = await this.blueprint720Service.autocomplete(
      query,
      currentUser
    );
    return blueprint720;
  }

  @Get("search-history")
  @UseGuards(AuthGuard())
  @UseInterceptors(LocaleInterceptor)
  public async findSearchHistory(
    @Query() query,
    @CurrentUser() currentUser: User
  ) {
    const searchHistory = await this.blueprint720Service.findSearchHistory(
      extractPaginate(query),
      currentUser
    );

    return searchHistory;
  }

  @Get("recently-viewed")
  @UseGuards(AuthGuard())
  @UseInterceptors(LocaleInterceptor)
  public async findRecent(@Query() query, @CurrentUser() currentUser: User) {
    return this.blueprint720Service.findRecent(
      extractPaginate(query),
      currentUser
    );
  }

  @Get("find-one")
  @UseInterceptors(LocaleInterceptor)
  public async findOne(
    @Query() query: Blueprint720SearchModel,
    @CurrentUser() currentUser?: User
  ) {
    const blueprint720 = await this.blueprint720Service.findOne(
      query,
      currentUser
    );
    return blueprint720;
  }

  @Get(":_id")
  @UseInterceptors(LocaleInterceptor)
  public async findById(
    @Param("_id") _id: string,
    @Query("populates") populates: string[],
    @CurrentUser() currentUser?: User
  ) {
    const pJ = await this.blueprint720Service.findById(
      _id,
      currentUser,
      populates
    );
    if (!pJ) throw new NotFoundException();
    return pJ;
  }

  @Post()
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "assetFiles" }], {
      fileFilter: fileFilterMultiple({
        fieldName: ["assetFiles"],
        extensions: ["jpg", "jpeg", "png", "gif", "mp4"],
      }),
    }),
    ParseFormDataInterceptor,
    LocaleInterceptor
  )
  public async create(
    @CurrentUser() currentUser: User,
    @Body() body: Blueprint720CreateDTOModel,
    @Query("populates") populates: string[],
    @UploadedFiles() files: { [key: string]: UploadedFile[] }
  ) {
    const createdItem = await this.blueprint720Service.create(
      {
        owner: currentUser._id.toHexString(),
        versioning: { version: 0, versionData: {}, versions: [] },
        rooms:
          body.rooms || this.blueprint720Service.getRoomsFromData(body.data),
        isSystem: false,
        ...body,
      },
      currentUser,
      files,
      populates
    );
    return this.blueprint720Service._populate(createdItem, populates);
  }

  @Put(":_id")
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "assetFiles" }], {
      fileFilter: fileFilterMultiple({
        fieldName: ["assetFiles"],
        extensions: ["jpg", "jpeg", "png", "gif", "mp4"],
      }),
    }),
    ParseFormDataInterceptor,
    LocaleInterceptor
  )
  public async update(
    @Param("_id") _id: string,
    @Body() body: Blueprint720UpdateModel,
    @Query("populates") populates: string[],
    @UploadedFiles() files: { [key: string]: UploadedFile[] },
    @CurrentUser() currentUser?: User
  ) {
    if (Object.keys(body).length === 0) {
      throw new Error("No update found");
    }

    // FIXME: files empty
    const updatedItem = await this.blueprint720Service.update(
      _id,
      {
        ...body,
        rooms:
          body.rooms || this.blueprint720Service.getRoomsFromData(body.data),
      },
      files,
      currentUser,
      populates
    );
    return this.blueprint720Service._populate(updatedItem, populates);
  }

  @Put(":_id/archive")
  @UseGuards(AuthGuard())
  public async archive(
    @Param("_id") _id: string,
    @Query("isArchived") isArchived?: boolean,
    @CurrentUser() currentUser?: User
  ) {
    return this.blueprint720Service.archive(_id, isArchived, currentUser);
  }

  @Delete(":_id")
  @UseGuards(AuthGuard())
  public async delete(
    @Param("_id") _id: string,
    @Query("populates") populates: string[],
    @CurrentUser() currentUser?: User
  ) {
    const deletedItem = await this.blueprint720Service.delete(_id, currentUser);
    return this.blueprint720Service._populate(deletedItem, populates);
  }

  @Post(":_id/clone")
  @UseGuards(AuthGuard())
  @UseInterceptors(LocaleInterceptor)
  public async cloneBlueprint(
    @Param("_id") _id: string,
    @Query("type") type: "full" | "layout",
    @Query("rooms") rooms: string[],
    @Query("version") version: number,
    @Query("save") save: boolean | string,
    @CurrentUser() currentUser: User
  ) {
    return this.blueprint720Service.cloneBlueprint(
      _id,
      {
        type,
        rooms: rooms?.length ? rooms : undefined,
        version: version || version === 0 ? version : undefined,
      },
      currentUser,
      {
        save: save === "" || save === "true",
      }
    );
  }

  @Post(":_id/user-permissions")
  @UseGuards(AuthGuard())
  public async addUserPermission(
    @Param("_id") _id: string,
    @Body() newPermission: UserPermissions,
    @CurrentUser() currentUser?: User
  ) {
    return this.blueprint720Service.addUserPermission(
      _id,
      newPermission,
      currentUser
    );
  }

  @Put(":_id/user-permissions/:permissionId")
  @UseGuards(AuthGuard())
  public async editUserPermission(
    @Param("_id") _id: string,
    @Param("permissionId") permissionId: string,
    @Body() editPermission: EditUserPermissionModel,
    @CurrentUser() currentUser?: User
  ) {
    if (Object.keys(editPermission).length === 0) {
      throw new Error("No update found");
    }

    return this.blueprint720Service.editUserPermission(
      _id,
      permissionId,
      editPermission,
      currentUser
    );
  }

  @Delete(":_id/user-permissions/:permissionId")
  @UseGuards(AuthGuard())
  public async removeUserPermission(
    @Param("_id") _id: string,
    @Param("permissionId") permissionId: string,
    @CurrentUser() currentUser?: User
  ) {
    return this.blueprint720Service.removeUserPermission(
      _id,
      permissionId,
      currentUser
    );
  }

  @Post(":_id/assets")
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "assetFiles" }], {
      fileFilter: fileFilterMultiple({
        fieldName: ["assetFiles"],
        extensions: ["jpg", "jpeg", "png", "gif", "mp4"],
      }),
    }),
    ParseFormDataInterceptor,
    LocaleInterceptor
  )
  public async addAssets(
    @Param("_id") _id: string,
    @Body("assets") assets: Asset[],
    @UploadedFiles() files?: { [key: string]: UploadedFile[] },
    @CurrentUser() currentUser?: User
  ) {
    return this.blueprint720Service.addAssets(_id, assets, files, currentUser);
  }

  @Put(":_id/assets/:assetId")
  @UseGuards(AuthGuard())
  @UseInterceptors(
    FileFieldsInterceptor([{ name: "assetFiles" }], {
      fileFilter: fileFilterMultiple({
        fieldName: ["assetFiles"],
        extensions: ["jpg", "jpeg", "png", "gif", "mp4"],
      }),
    }),
    ParseFormDataInterceptor,
    LocaleInterceptor
  )
  public updateAsset(
    @Param("_id") _id: string,
    @Param("assetId") assetId: string,
    @Body("asset") updateAsset: UpdateAssetModel,
    @UploadedFiles() files?: { [key: string]: UploadedFile[] },
    @CurrentUser() currentUser?: User
  ) {
    if (Object.keys(updateAsset).length === 0) {
      throw new Error("No update found");
    }

    return this.blueprint720Service.updateAsset(
      _id,
      assetId,
      updateAsset,
      files,
      currentUser
    );
  }

  @Delete(":_id/assets")
  @UseGuards(AuthGuard())
  public deleteAssets(
    @Param("_id") _id: string,
    @Query("assetIds") assetIds: string[],
    @CurrentUser() currentUser?: User
  ) {
    return this.blueprint720Service.deleteAssets(_id, assetIds, currentUser);
  }

  @Put(":_id/publish-version")
  @UseGuards(AuthGuard())
  public async publishVersion(
    @Param("_id") _id: string,
    @CurrentUser() currentUser?: User
  ) {
    return this.blueprint720Service.publishVersion(_id, currentUser);
  }

  @Get(":_id/get-data-by-version")
  @UseGuards(AuthGuard())
  public async getDataByVersion(
    @Param("_id") _id: string,
    @Query("version") version: number
  ) {
    return this.blueprint720Service.getDataByVersion(_id, version);
  }

  @Put(":_id/add-bookmark")
  @UseGuards(AuthGuard())
  public async addBookmark(
    @Param("_id") _id: string,
    @CurrentUser() currentUser: User
  ) {
    return this.blueprint720Service.addBookmark(_id, currentUser);
  }

  @Put(":_id/remove-bookmark")
  @UseGuards(AuthGuard())
  public async removeBookmark(
    @Param("_id") _id: string,
    @CurrentUser() currentUser: User
  ) {
    return this.blueprint720Service.removeBookmark(_id, currentUser);
  }

  @Get(":_id/shake-data")
  public async(
    @Param("_id") _id: string,
    @Query() filters: Blueprint720ShakeDataFiltersModel,
    @CurrentUser() currentUser: User
  ) {
    console.log("currentUser", currentUser._id.toHexString());

    return this.blueprint720Service.shakeData(_id, filters, currentUser);
  }
}
