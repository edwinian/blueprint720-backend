import { HttpModule, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { Blueprint720Schema } from "./schemas/blueprint720.schemas";
import { Blueprint720Controller } from "./blueprint720.controller";
import { Blueprint720Service } from "./blueprint720.service";
import { Blueprint720LayoutGroupingModule } from "../Blueprint720LayoutGrouping/blueprint720LayoutGrouping.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Blueprint720s", schema: Blueprint720Schema },
    ]),
    Blueprint720LayoutGroupingModule,
    HttpModule,
  ],
  controllers: [Blueprint720Controller],
  providers: [Blueprint720Service],
  exports: [Blueprint720Service],
})
export class Blueprint720Module {}
