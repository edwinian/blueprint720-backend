import { Address, Asset } from "./blueprint720";

export interface Suggestion {
  searchTerm: Address["properties"]["text"] | string;
  text: string;
  subtext: string;
  icon: Asset["url"] | string;
}
