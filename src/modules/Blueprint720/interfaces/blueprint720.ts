import { ObjectID } from "mongodb";
import { Document, PaginatesModel } from "mongoose";
import {
  Blueprint720LayoutGrouping,
  Blueprint720LayoutGroupingDoc,
} from "src/modules/Blueprint720LayoutGrouping/interfaces/blueprint720LayoutGrouping";
import { LocalizeString } from "../../../core/mongo/localize";
import { User } from "../../../modules/User/interfaces/user";

export interface UserPermissions {
  _id: ObjectID;
  user: User["_id"];
  read?: boolean;
  edit?: boolean;
  delete?: boolean;
}

export interface CreateAndUpdateUserPermissions {
  user: User["_id"];
  read?: boolean;
  edit?: boolean;
  delete?: boolean;
}
export interface Version {
  no: number;
  delta?: Object;
}

export interface Versioning {
  version: number; // always the latest published

  versionData?: Object; // always the latest published

  versions: Version[];
}

export interface Room {
  roomId: string;
  types: string[];
  spaces: string[];
}

export interface FloorPlan {
  _id: string;
  floorType: string;
  space: string;
  isClose: boolean;
  corners: any[];
  walls: any[];
  floormat: any;
  CeillingMat: any;
}

export interface Asset {
  _id: ObjectID;
  type: "render" | "layout" | "360" | "video";
  file: string;
  url: string;
  large?: string;
  medium?: string;
  small?: string;
  name?: LocalizeString;
  description?: LocalizeString;
  position360?: { x: number; y: number; z: number };
  roomId?: string;
}

export interface Address {
  type: string;
  geometry?: {
    type: string;
    coordinates: number[];
  };
  properties: {
    text?: string;
    country?: string;
    state?: string;
    city?: string;
    district?: string;
    administrative_area_1?: string;
    estate?: string;
    building?: string;
    floor?: number;
    unit?: string;
    elevation_m?: number;
  };
}

export interface Data {
  floorplan?: FloorPlan[];
  Save_Furniture?: any[];
}

export interface BookmarkedUser {
  user: ObjectID;
  time: Date;
}

export interface Blueprint720 {
  /**
   * unique ID for document
   */
  _id: ObjectID;

  externalId?: string;

  name: LocalizeString;

  bookmarkedUsers: BookmarkedUser[];

  layoutGroupings?: Array<string>;

  isLayout?: boolean;

  isNewEstate?: boolean;

  isRecommended?: boolean;

  clonedFrom?: ObjectID | Blueprint720;

  versioning: Versioning;

  data: Data;

  rooms: Room[];

  owner: User["_id"] | User;

  userPermissions: UserPermissions[];

  address?: Address;

  buildingComplexType?: string;

  assets: Asset[];

  yearBuilt?: number;

  shapes?: string[];

  sqft?: number;

  sqftActual?: number;

  styles?: string[];

  ideaDescription?: LocalizeString;

  description?: LocalizeString;

  isArchived?: boolean;

  isPublic?: boolean;

  isSystem?: boolean;

  /**
   * create time of this document
   */
  createdAt?: Date;

  /**
   * update time of this document
   */
  updatedAt?: Date;
}

export type Blueprint720Doc = Blueprint720 & Document;

export type Blueprint720Model = PaginatesModel<Blueprint720Doc> & Document;
